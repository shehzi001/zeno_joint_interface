#include <stdio.h>
#include <string>
#include <errno.h>
#include <limits.h>
#include <assert.h>
#include <stdlib.h>
#include <vector>
#include <map>
#include "oru_robot_interface.h"
#include <zeno_joint_ids.h>
#include <joint_limits.h>
#include <time.h>


std::string connection_ip = "127.0.0.1";
const int connection_port = 19997;
void sleepcp(int milliseconds) // cross-platform sleep function
{
  clock_t time_end;
  time_end = clock() + milliseconds * CLOCKS_PER_SEC/1000;
  while (clock() < time_end)
  {
  }
}

void get_walk_command(std::vector<double> & walk_command, int walk_command_index);
int main(int argc, char *argv[])
{
    boost::shared_ptr<oru_robot_interface> robot_interface_;

    JointLimits joint_limits;

    std::vector<std::string> joint_names_;
    std::vector<double> init_joint_angles;

    joint_names_.resize(JointLimits::JOINTS_NUM);    
    joint_names_[JointLimits::HEAD_PITCH]       = "neck_pitch";
    joint_names_[JointLimits::HEAD_YAW]         = "neck_yaw";
    joint_names_[JointLimits::HEAD_ROLL]        = "neck_roll";

    joint_names_[JointLimits::L_ANKLE_PITCH]    = "left_ankle_pitch";
    joint_names_[JointLimits::L_ANKLE_ROLL]     = "left_ankle_roll";
    joint_names_[JointLimits::L_ELBOW_ROLL]     = "left_elbow_roll";
    joint_names_[JointLimits::L_ELBOW_YAW]      = "left_elbow_yaw";
    joint_names_[JointLimits::L_HIP_PITCH]      = "left_hip_pitch";
    joint_names_[JointLimits::L_HIP_ROLL]       = "left_hip_roll";
    joint_names_[JointLimits::L_HIP_YAW]        = "left_hip_yaw";
    joint_names_[JointLimits::L_KNEE_PITCH]     = "left_knee_pitch";
    joint_names_[JointLimits::L_SHOULDER_PITCH] = "left_shoulder_pitch";
    joint_names_[JointLimits::L_SHOULDER_ROLL]  = "left_shoulder_roll";
    joint_names_[JointLimits::L_WRIST_YAW]      = "left_wrist_yaw";

    joint_names_[JointLimits::R_ANKLE_PITCH]    = "right_ankle_pitch";
    joint_names_[JointLimits::R_ANKLE_ROLL]     = "right_ankle_roll";
    joint_names_[JointLimits::R_ELBOW_ROLL]     = "right_elbow_roll";
    joint_names_[JointLimits::R_ELBOW_YAW]      = "right_elbow_yaw";
    joint_names_[JointLimits::R_HIP_PITCH]      = "right_hip_pitch";
    joint_names_[JointLimits::R_HIP_ROLL]       = "right_hip_roll";
    joint_names_[JointLimits::R_HIP_YAW]        = "right_hip_yaw";
    joint_names_[JointLimits::R_KNEE_PITCH]     = "right_knee_pitch";
    joint_names_[JointLimits::R_SHOULDER_PITCH] = "right_shoulder_pitch";
    joint_names_[JointLimits::R_SHOULDER_ROLL]  = "right_shoulder_roll";
    joint_names_[JointLimits::R_WRIST_YAW]      = "right_wrist_yaw";

    joint_names_[JointLimits::TORSO_YAW]         = "waist";

    init_joint_angles.resize(JointLimits::JOINTS_NUM);
        
    init_joint_angles[JointLimits::L_HIP_ROLL]       =       0.0;
    init_joint_angles[JointLimits::L_HIP_YAW]        =       0.0;
    init_joint_angles[JointLimits::L_HIP_PITCH]      =       -0.436332;
    init_joint_angles[JointLimits::L_KNEE_PITCH]     =       0.436332;
    init_joint_angles[JointLimits::L_ANKLE_PITCH]    =       -0.436332/4;
    init_joint_angles[JointLimits::L_ANKLE_ROLL]     =       0.0;

    init_joint_angles[JointLimits::R_HIP_ROLL]       =       0.0;
    init_joint_angles[JointLimits::R_HIP_YAW]        =       0.0;
    init_joint_angles[JointLimits::R_HIP_PITCH]      =       -0.436332;
    init_joint_angles[JointLimits::R_KNEE_PITCH]     =       0.436332;
    init_joint_angles[JointLimits::R_ANKLE_PITCH]    =       -0.436332/4;
    init_joint_angles[JointLimits::R_ANKLE_ROLL]     =       0.0;

    init_joint_angles[JointLimits::L_SHOULDER_PITCH] =       0.78539;
    init_joint_angles[JointLimits::L_SHOULDER_ROLL]  =       0.78539;
    init_joint_angles[JointLimits::L_ELBOW_ROLL]     =       0.0;
    init_joint_angles[JointLimits::L_ELBOW_YAW]      =       -0.78539;
    init_joint_angles[JointLimits::L_WRIST_YAW]      =       0.0;

    init_joint_angles[JointLimits::R_SHOULDER_PITCH] =       0.78539;
    init_joint_angles[JointLimits::R_SHOULDER_ROLL]  =       -0.78539;
    init_joint_angles[JointLimits::R_ELBOW_ROLL]     =       0.0;
    init_joint_angles[JointLimits::R_ELBOW_YAW]      =       0.78539;

    init_joint_angles[JointLimits::R_WRIST_YAW]      =       0.0;

    init_joint_angles[JointLimits::HEAD_PITCH]       =       0.0;
    init_joint_angles[JointLimits::HEAD_YAW]         =       0.0;
    init_joint_angles[JointLimits::HEAD_ROLL]        =       0.0;

    init_joint_angles[JointLimits::TORSO_YAW]        =       0.0;
     
    robot_interface_.reset(new oru_robot_interface(connection_ip.c_str(), connection_port, joint_names_));
    sleep(1);
    while(1) {
        int index[1];
        std::vector<double> joint_positions;
        joint_positions.resize(1);

        double position_in = 0.0;
        std::cin >> position_in;
        double position_out;
        joint_limits.convertToActualValue(position_in, position_out, JointLimits::R_SHOULDER_ROLL);

        index[0] = (int)JointLimits::R_SHOULDER_ROLL;
        joint_positions[0] = position_out;
        std::cout << "\n joint command = " << position_out << std::endl;
        robot_interface_->sendPositionsCommand(index, joint_positions, 1);
        sleep(2);
        std::vector<double> joint_positions_read;
        //joint_positions_read.clear();
        robot_interface_->readJointPositions(index, 1 , joint_positions_read);

        joint_limits.convertToModelValue(joint_positions_read[0], position_out, JointLimits::R_SHOULDER_ROLL);
        std::cout << "\n joint state = " << position_out << std::endl;
        std::cout << "============================" << std::endl;
    }
    return 0;
}
