#include <stdio.h>
#include <string>
#include <errno.h>
#include <limits.h>
#include <assert.h>
#include <stdlib.h>
#include <vector>
#include <map>
#include "oru_robot_interface.h"
#include <zeno_joint_ids.h>
#include <joint_limits.h>
#include <time.h>


std::string connection_ip = "127.0.0.1";
const int connection_port = 19997;
void sleepcp(int milliseconds) // cross-platform sleep function
{
  clock_t time_end;
  time_end = clock() + milliseconds * CLOCKS_PER_SEC/1000;
  while (clock() < time_end)
  {
  }
}

void get_walk_command(std::vector<double> & walk_command, int walk_command_index);
int main(int argc, char *argv[])
{
    boost::shared_ptr<oru_robot_interface> robot_interface_;

    JointLimits joint_limits;

    std::vector<std::string> joint_names_;
    std::vector<double> init_joint_angles;

    joint_names_.resize(JointLimits::JOINTS_NUM);    
    joint_names_[JointLimits::HEAD_PITCH]       = "neck_pitch";
    joint_names_[JointLimits::HEAD_YAW]         = "neck_yaw";
    joint_names_[JointLimits::HEAD_ROLL]        = "neck_roll";

    joint_names_[JointLimits::L_ANKLE_PITCH]    = "left_ankle_pitch";
    joint_names_[JointLimits::L_ANKLE_ROLL]     = "left_ankle_roll";
    joint_names_[JointLimits::L_ELBOW_ROLL]     = "left_elbow_roll";
    joint_names_[JointLimits::L_ELBOW_YAW]      = "left_elbow_yaw";
    joint_names_[JointLimits::L_HIP_PITCH]      = "left_hip_pitch";
    joint_names_[JointLimits::L_HIP_ROLL]       = "left_hip_roll";
    joint_names_[JointLimits::L_HIP_YAW]        = "left_hip_yaw";
    joint_names_[JointLimits::L_KNEE_PITCH]     = "left_knee_pitch";
    joint_names_[JointLimits::L_SHOULDER_PITCH] = "left_shoulder_pitch";
    joint_names_[JointLimits::L_SHOULDER_ROLL]  = "left_shoulder_roll";
    joint_names_[JointLimits::L_WRIST_YAW]      = "left_wrist_yaw";

    joint_names_[JointLimits::R_ANKLE_PITCH]    = "right_ankle_pitch";
    joint_names_[JointLimits::R_ANKLE_ROLL]     = "right_ankle_roll";
    joint_names_[JointLimits::R_ELBOW_ROLL]     = "right_elbow_roll";
    joint_names_[JointLimits::R_ELBOW_YAW]      = "right_elbow_yaw";
    joint_names_[JointLimits::R_HIP_PITCH]      = "right_hip_pitch";
    joint_names_[JointLimits::R_HIP_ROLL]       = "right_hip_roll";
    joint_names_[JointLimits::R_HIP_YAW]        = "right_hip_yaw";
    joint_names_[JointLimits::R_KNEE_PITCH]     = "right_knee_pitch";
    joint_names_[JointLimits::R_SHOULDER_PITCH] = "right_shoulder_pitch";
    joint_names_[JointLimits::R_SHOULDER_ROLL]  = "right_shoulder_roll";
    joint_names_[JointLimits::R_WRIST_YAW]      = "right_wrist_yaw";

    joint_names_[JointLimits::TORSO_YAW]         = "waist";

    init_joint_angles.resize(JointLimits::JOINTS_NUM);
        
    init_joint_angles[JointLimits::L_HIP_ROLL]       =       0.0;
    init_joint_angles[JointLimits::L_HIP_YAW]        =       0.0;
    init_joint_angles[JointLimits::L_HIP_PITCH]      =       -0.436332;
    init_joint_angles[JointLimits::L_KNEE_PITCH]     =       0.436332;
    init_joint_angles[JointLimits::L_ANKLE_PITCH]    =       -0.436332/4;
    init_joint_angles[JointLimits::L_ANKLE_ROLL]     =       0.0;

    init_joint_angles[JointLimits::R_HIP_ROLL]       =       0.0;
    init_joint_angles[JointLimits::R_HIP_YAW]        =       0.0;
    init_joint_angles[JointLimits::R_HIP_PITCH]      =       -0.436332;
    init_joint_angles[JointLimits::R_KNEE_PITCH]     =       0.436332;
    init_joint_angles[JointLimits::R_ANKLE_PITCH]    =       -0.436332/4;
    init_joint_angles[JointLimits::R_ANKLE_ROLL]     =       0.0;

    init_joint_angles[JointLimits::L_SHOULDER_PITCH] =       0.78539;
    init_joint_angles[JointLimits::L_SHOULDER_ROLL]  =       0.78539;
    init_joint_angles[JointLimits::L_ELBOW_ROLL]     =       0.0;
    init_joint_angles[JointLimits::L_ELBOW_YAW]      =       -0.78539;
    init_joint_angles[JointLimits::L_WRIST_YAW]      =       0.0;

    init_joint_angles[JointLimits::R_SHOULDER_PITCH] =       0.78539;
    init_joint_angles[JointLimits::R_SHOULDER_ROLL]  =       -0.78539;
    init_joint_angles[JointLimits::R_ELBOW_ROLL]     =       0.0;
    init_joint_angles[JointLimits::R_ELBOW_YAW]      =       0.78539;

    init_joint_angles[JointLimits::R_WRIST_YAW]      =       0.0;

    init_joint_angles[JointLimits::HEAD_PITCH]       =       0.0;
    init_joint_angles[JointLimits::HEAD_YAW]         =       0.0;
    init_joint_angles[JointLimits::HEAD_ROLL]        =       0.0;

    init_joint_angles[JointLimits::TORSO_YAW]        =       0.0;
     
    robot_interface_.reset(new oru_robot_interface(connection_ip.c_str(), connection_port, joint_names_));
    sleep(1);

    for(int j=0; j <= 285; j++) {
        double  a=0.0;
        //std::cout<< "enter value: " << std::endl;
        //std::cin >> a; 

        //if (a > 2000.0)
        //    break;
        //init_joint_angles[JointLimits::L_ELBOW_YAW] = a;
        int index[joint_names_.size()];
        std::vector<double> joint_positions;
        joint_positions.resize(joint_names_.size());

        std::vector<double> actual_angles;

        std::vector<double> walk_command;

         get_walk_command(walk_command,j); 

        for (int cmd=0; cmd< walk_command.size(); cmd++) {
           //init_joint_angles[cmd] = walk_command[cmd];
        }

	    std::cout << "init_joint_angles:{";
	    for (int i=0; i< JointLimits::JOINTS_NUM; i++)
	    	std::cout << init_joint_angles[i] << ",";

	    std::cout << "}"<< std::endl;

        joint_limits.convertToActualValues(init_joint_angles, actual_angles);

	    std::cout << "actual_angles:{";
	    for (int i=0; i< JointLimits::JOINTS_NUM; i++)
	    	std::cout << actual_angles[i] << ",";

	    std::cout << "}"<< std::endl;

        for (int i=0; i< joint_names_.size(); i++) {
            index[i] = i;
            joint_positions[i] = actual_angles[i];
        }


        robot_interface_->sendPositionsCommand(index, joint_positions, joint_names_.size());
        sleepcp(300);

        std::vector<double> joint_positions_read;
	    /*int index_read[JointLimits::JOINTS_NUM];
	    for (int i= 0; i< joint_names_.size(); i++)
	        index_read[i] = i;*/
	    joint_positions_read.clear();
	    robot_interface_->readJointPositions(index, JointLimits::JOINTS_NUM , joint_positions_read);
	    std::cout << "joint_positions_read:{";
	    for (int i=0; i< JointLimits::JOINTS_NUM; i++)
	    	std::cout << joint_positions_read[i] << ",";

	    std::cout << "}"<< std::endl;

	    std::vector<double> model_angles;
	    joint_limits.convertToModelValues(joint_positions_read, model_angles);

        std::vector<double> error_positions;
        error_positions.resize(joint_names_.size());

	    for (int i=0; i< JointLimits::JOINTS_NUM; i++) {
	    	error_positions[i] = init_joint_angles[i]- model_angles[i];
	    	if ((error_positions[i] > 0.00000000001) && (error_positions[i] < 0.00000000001))
	    	   error_positions[i] = 0.0;
	    }

	    std::cout << "model_angles:{";
	    for (int i=0; i< JointLimits::JOINTS_NUM; i++)
	    	std::cout << model_angles[i] << ",";

	    std::cout << "}"<< std::endl;
	    sleepcp(300); break;
    } 

    return 0;
}
void get_walk_command(std::vector<double> & walk_command, int walk_command_index)
{
    int len = 12;
    switch(walk_command_index)
    {
	case 0: {
	 	 double q[] = {1.46197e-07, 1.88616e-08, -0.436333, 0.436335, -0.109079, -1.47071e-07, 1.46197e-07, 1.88616e-08, -0.436333, 0.436335, -0.109079, -1.47071e-07}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 1: {
	 	 double q[] = {5.96897e-07, 7.68502e-08, -0.436335, 0.436343, -0.109071, -6.00465e-07, 5.96897e-07, 7.68502e-08, -0.436335, 0.436343, -0.109071, -6.00465e-07}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 2: {
	 	 double q[] = {1.76785e-06, 2.26498e-07, -0.436339, 0.436356, -0.109055, -1.77842e-06, 1.76785e-06, 2.26498e-07, -0.436339, 0.436356, -0.109055, -1.77842e-06}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 3: {
	 	 double q[] = {4.01368e-06, 5.12531e-07, -0.436344, 0.436378, -0.10903, -4.03765e-06, 4.01368e-06, 5.12531e-07, -0.436344, 0.436378, -0.10903, -4.03765e-06}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 4: {
	 	 double q[] = {7.81867e-06, 9.95595e-07, -0.436351, 0.43641, -0.108994, -7.8653e-06, 7.81867e-06, 9.95595e-07, -0.436351, 0.43641, -0.108994, -7.8653e-06}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 5: {
	 	 double q[] = {1.35916e-05, 1.72663e-06, -0.43636, 0.436452, -0.108946, -1.36726e-05, 1.35916e-05, 1.72663e-06, -0.43636, 0.436451, -0.108946, -1.36726e-05}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 6: {
	 	 double q[] = {2.21151e-05, 2.80263e-06, -0.43637, 0.436505, -0.108885, -2.22466e-05, 2.21151e-05, 2.80263e-06, -0.43637, 0.436504, -0.108884, -2.22466e-05}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 7: {
	 	 double q[] = {3.37194e-05, 4.26376e-06, -0.436383, 0.43657, -0.108809, -3.39193e-05, 3.37194e-05, 4.26376e-06, -0.436383, 0.436569, -0.108809, -3.39193e-05}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 8: {
	 	 double q[] = {4.87396e-05, 6.1502e-06, -0.436397, 0.436647, -0.10872, -4.90278e-05, 4.87396e-05, 6.1502e-06, -0.436397, 0.436646, -0.108719, -4.90278e-05}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 9: {
	 	 double q[] = {6.70891e-05, 8.44912e-06, -0.436413, 0.436736, -0.108616, -6.74844e-05, 6.70891e-05, 8.44912e-06, -0.436413, 0.436735, -0.108615, -6.74844e-05}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 10: {
	 	 double q[] = {8.80535e-05, 1.10695e-05, -0.43643, 0.436838, -0.108498, -8.85701e-05, 8.80534e-05, 1.10695e-05, -0.43643, 0.436836, -0.108496, -8.85701e-05}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 11: {
	 	 double q[] = {0.000110679, 1.38907e-05, -0.436449, 0.436952, -0.108365, -0.000111325, 0.000110679, 1.38907e-05, -0.436449, 0.43695, -0.108363, -0.000111325}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 12: {
	 	 double q[] = {0.000132839, 1.66477e-05, -0.436468, 0.437077, -0.108217, -0.000133611, 0.000132839, 1.66477e-05, -0.436468, 0.437075, -0.108215, -0.000133611}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 13: {
	 	 double q[] = {0.000152408, 1.90771e-05, -0.436489, 0.437215, -0.108056, -0.000153288, 0.000152408, 1.90771e-05, -0.436489, 0.437213, -0.108054, -0.000153288}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 14: {
	 	 double q[] = {0.000165618, 2.0716e-05, -0.43651, 0.437364, -0.107881, -0.000166569, 0.000165618, 2.0716e-05, -0.43651, 0.437361, -0.107878, -0.000166569}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 15: {
	 	 double q[] = {0.000168988, 2.11409e-05, -0.436532, 0.437524, -0.107693, -0.000169952, 0.000168988, 2.11409e-05, -0.436532, 0.437521, -0.10769, -0.000169952}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 16: {
	 	 double q[] = {0.000156998, 1.96832e-05, -0.436554, 0.437694, -0.107491, -0.000157886, 0.000156998, 1.96832e-05, -0.436554, 0.437692, -0.107488, -0.000157886}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 17: {
	 	 double q[] = {0.000124738, 1.57576e-05, -0.436575, 0.437875, -0.107277, -0.000125437, 0.000124738, 1.57576e-05, -0.436575, 0.437873, -0.107275, -0.000125437}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 18: {
	 	 double q[] = {6.49143e-05, 8.4965e-06, -0.436597, 0.438065, -0.10705, -6.52732e-05, 6.49143e-05, 8.4965e-06, -0.436597, 0.438063, -0.107049, -6.52732e-05}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 19: {
	 	 double q[] = {-2.87843e-05, -2.83566e-06, -0.436617, 0.438264, -0.106812, 2.89483e-05, -2.87843e-05, -2.83566e-06, -0.436617, 0.438264, -0.106812, 2.89482e-05}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 20: {
	 	 double q[] = {-0.000165355, -1.92871e-05, -0.436636, 0.438472, -0.106563, 0.000166268, -0.000165355, -1.92871e-05, -0.436636, 0.438474, -0.106564, 0.000166268}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 21: {
	 	 double q[] = {-0.000352407, -4.17203e-05, -0.436654, 0.438687, -0.106302, 0.00035433, -0.000352407, -4.17203e-05, -0.436654, 0.438692, -0.106307, 0.00035433}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 22: {
	 	 double q[] = {-0.000600497, -7.13352e-05, -0.43667, 0.43891, -0.106031, 0.000603738, -0.000600497, -7.13352e-05, -0.43667, 0.438918, -0.106039, 0.000603738}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 23: {
	 	 double q[] = {-0.000918347, -0.000109088, -0.436683, 0.439139, -0.105751, 0.000923249, -0.000918348, -0.000109088, -0.436683, 0.439151, -0.105764, 0.000923249}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 24: {
	 	 double q[] = {-0.00131786, -0.000156292, -0.436692, 0.439372, -0.105461, 0.00132481, -0.00131786, -0.000156292, -0.436692, 0.439391, -0.10548, 0.00132482}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 25: {
	 	 double q[] = {-0.00180861, -0.000213953, -0.436696, 0.43961, -0.105163, 0.00181805, -0.00180862, -0.000213954, -0.436696, 0.439637, -0.10519, 0.00181805}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 26: {
	 	 double q[] = {-0.00240358, -0.000283453, -0.436693, 0.439851, -0.104858, 0.00241597, -0.00240358, -0.000283454, -0.436693, 0.439887, -0.104893, 0.00241597}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 27: {
	 	 double q[] = {-0.00311287, -0.000365796, -0.436681, 0.440092, -0.104545, 0.00312872, -0.00311287, -0.000365796, -0.436681, 0.440139, -0.104592, 0.00312872}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 28: {
	 	 double q[] = {-0.00395016, -0.000462368, -0.436658, 0.440333, -0.104228, 0.00397002, -0.00395017, -0.000462369, -0.436658, 0.440392, -0.104288, 0.00397003}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 29: {
	 	 double q[] = {-0.00492574, -0.000574109, -0.436619, 0.440569, -0.103906, 0.00495018, -0.00492575, -0.00057411, -0.436619, 0.440644, -0.103981, 0.00495019}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 30: {
	 	 double q[] = {-0.00605356, -0.000702337, -0.43656, 0.440799, -0.103582, 0.00608319, -0.00605358, -0.000702339, -0.43656, 0.440892, -0.103674, 0.00608321}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 31: {
	 	 double q[] = {-0.0073436, -0.000847839, -0.436477, 0.441019, -0.103256, 0.00737904, -0.00734363, -0.000847843, -0.436477, 0.441132, -0.103369, 0.00737908}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 32: {
	 	 double q[] = {-0.00881009, -0.00101183, -0.436361, 0.441225, -0.102932, 0.008852, -0.00881014, -0.00101183, -0.436361, 0.441361, -0.103068, 0.00885205}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 33: {
	 	 double q[] = {-0.010548, -0.00119833, -0.436129, 0.441407, -0.102589, 0.0105973, -0.0105481, -0.00119834, -0.436129, 0.441565, -0.102748, 0.0105974}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 34: {
	 	 double q[] = {-0.0124096, -0.00140078, -0.435917, 0.441565, -0.102276, 0.0124666, -0.0124097, -0.00140079, -0.435917, 0.441751, -0.102462, 0.0124667}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 35: {
	 	 double q[] = {-0.0144783, -0.0016229, -0.435644, 0.441691, -0.101972, 0.0145437, -0.0144784, -0.00162291, -0.435644, 0.441907, -0.102188, 0.0145439}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 36: {
	 	 double q[] = {-0.0167658, -0.00186499, -0.435297, 0.441778, -0.101681, 0.0168403, -0.016766, -0.00186501, -0.435297, 0.442026, -0.101929, 0.0168405}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 37: {
	 	 double q[] = {-0.0192771, -0.00212639, -0.434862, 0.441816, -0.101407, 0.0193613, -0.0192773, -0.00212641, -0.434862, 0.442098, -0.101689, 0.0193615}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 38: {
	 	 double q[] = {-0.0220211, -0.00240657, -0.434321, 0.441798, -0.101154, 0.0221155, -0.0220214, -0.0024066, -0.434321, 0.442115, -0.101471, 0.0221158}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 39: {
	 	 double q[] = {-0.0249995, -0.00270395, -0.433658, 0.441712, -0.100926, 0.0251046, -0.0249998, -0.00270398, -0.433658, 0.442064, -0.101279, 0.0251049}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 40: {
	 	 double q[] = {-0.0282174, -0.00301695, -0.432852, 0.441547, -0.100729, 0.0283337, -0.0282178, -0.00301699, -0.432852, 0.441935, -0.101117, 0.0283341}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 41: {
	 	 double q[] = {-0.0316723, -0.00334279, -0.431884, 0.441293, -0.100568, 0.0318001, -0.0316729, -0.00334284, -0.431884, 0.441714, -0.100988, 0.0318006}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 42: {
	 	 double q[] = {-0.0353651, -0.00367863, -0.430731, 0.440937, -0.100448, 0.0355047, -0.0353658, -0.00367869, -0.430731, 0.441386, -0.100897, 0.0355053}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 43: {
	 	 double q[] = {-0.0392882, -0.00402037, -0.429373, 0.440466, -0.100376, 0.0394397, -0.039289, -0.00402044, -0.429373, 0.440937, -0.100848, 0.0394405}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 44: {
	 	 double q[] = {-0.0434372, -0.00436373, -0.427785, 0.439866, -0.100358, 0.0436007, -0.0434381, -0.00436381, -0.427785, 0.440352, -0.100844, 0.0436016}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 45: {
	 	 double q[] = {-0.0477987, -0.00470329, -0.425949, 0.439124, -0.1004, 0.0479743, -0.0477997, -0.00470337, -0.425949, 0.439614, -0.100889, 0.0479752}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 46: {
	 	 double q[] = {-0.0523623, -0.0050335, -0.423842, 0.438228, -0.100508, 0.0525498, -0.0523633, -0.00503358, -0.423842, 0.438706, -0.100987, 0.0525508}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 47: {
	 	 double q[] = {-0.057108, -0.00534788, -0.421449, 0.437164, -0.100691, 0.0573074, -0.0571091, -0.00534797, -0.421449, 0.437614, -0.10114, 0.0573084}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 48: {
	 	 double q[] = {-0.0620185, -0.00564009, -0.418752, 0.435921, -0.100953, 0.0622297, -0.0620195, -0.00564017, -0.418752, 0.436321, -0.101353, 0.0622307}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 49: {
	 	 double q[] = {-0.0670666, -0.00590334, -0.415744, 0.434489, -0.101302, 0.0672895, -0.0670674, -0.00590341, -0.415744, 0.434813, -0.101627, 0.0672904}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 50: {
	 	 double q[] = {-0.0722272, -0.00613143, -0.412419, 0.432858, -0.101744, 0.0724622, -0.0722279, -0.00613148, -0.412419, 0.43308, -0.101966, 0.0724628}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 51: {
	 	 double q[] = {-0.0774654, -0.00631857, -0.408779, 0.431024, -0.102286, 0.0777126, -0.0774656, -0.00631859, -0.408779, 0.431111, -0.102372, 0.0777128}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 52: {
	 	 double q[] = {-0.0827478, -0.00646021, -0.404834, 0.428984, -0.102931, 0.083008, -0.0827475, -0.00646019, -0.404834, 0.428902, -0.102849, 0.0830077}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 53: {
	 	 double q[] = {-0.0880309, -0.00655335, -0.400605, 0.426741, -0.103687, 0.0883049, -0.0880298, -0.00655327, -0.400605, 0.426454, -0.103401, 0.0883039}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 54: {
	 	 double q[] = {-0.0932729, -0.00659707, -0.39612, 0.4243, -0.104557, 0.0935622, -0.093271, -0.00659692, -0.39612, 0.423772, -0.10403, 0.0935603}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 55: {
	 	 double q[] = {-0.0984211, -0.00659327, -0.391424, 0.421675, -0.105547, 0.0987274, -0.098418, -0.00659303, -0.391424, 0.420872, -0.104744, 0.0987243}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 56: {
	 	 double q[] = {-0.103426, -0.00654667, -0.38657, 0.418886, -0.106659, 0.103751, -0.103421, -0.00654634, -0.38657, 0.417775, -0.105548, 0.103747}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 57: {
	 	 double q[] = {-0.108224, -0.00646605, -0.381631, 0.415961, -0.1079, 0.108571, -0.108218, -0.0064656, -0.381631, 0.414515, -0.106454, 0.108565}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 58: {
	 	 double q[] = {-0.112759, -0.00636313, -0.376687, 0.412935, -0.109272, 0.113129, -0.112751, -0.00636255, -0.376687, 0.411134, -0.107471, 0.113122}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 59: {
	 	 double q[] = {-0.0821904, -0.0146519, -0.454659, 0.447871, -0.102564, 0.111274, -0.0882721, -0.0153192, -0.454659, 0.445831, -0.100432, 0.117392}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 60: {
	 	 double q[] = {-0.0493197, -0.0220871, -0.522882, 0.478015, -0.0954292, 0.108322, -0.0612768, -0.0237736, -0.522882, 0.464623, -0.0817639, 0.120394}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 61: {
	 	 double q[] = {-0.0158309, -0.0273915, -0.581927, 0.505103, -0.0893429, 0.104601, -0.0332784, -0.030312, -0.581927, 0.470617, -0.0543537, 0.122284}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 62: {
	 	 double q[] = {0.0166122, -0.0307279, -0.632163, 0.529964, -0.0850456, 0.100405, -0.00582117, -0.0349676, -0.632163, 0.467281, -0.0216257, 0.123224}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 63: {
	 	 double q[] = {0.0466826, -0.032584, -0.674266, 0.552563, -0.0825284, 0.0959993, 0.019856, -0.0381239, -0.674266, 0.457172, 0.0138109, 0.123376}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 64: {
	 	 double q[] = {0.07365, -0.0336229, -0.708617, 0.573731, -0.0822892, 0.0915021, 0.0430781, -0.0403465, -0.708617, 0.443442, 0.0491309, 0.122784}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 65: {
	 	 double q[] = {0.0971448, -0.0344424, -0.735969, 0.593594, -0.0842456, 0.0869972, 0.0635093, -0.0421704, -0.735969, 0.428038, 0.0825988, 0.121485}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 66: {
	 	 double q[] = {0.116884, -0.0354005, -0.757108, 0.611466, -0.0877958, 0.0825517, 0.0808948, -0.0439249, -0.757108, 0.411493, 0.113605, 0.119509}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 67: {
	 	 double q[] = {0.132873, -0.0369139, -0.772322, 0.62776, -0.0931161, 0.0781586, 0.0952469, -0.0459914, -0.772322, 0.395168, 0.141036, 0.116833}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 68: {
	 	 double q[] = {0.145109, -0.039298, -0.781812, 0.642535, -0.100198, 0.0738107, 0.106567, -0.0486659, -0.781812, 0.379728, 0.164304, 0.113438}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 69: {
	 	 double q[] = {0.153567, -0.04285, -0.785547, 0.655845, -0.109093, 0.0695118, 0.114831, -0.0522326, -0.785547, 0.365635, 0.182957, 0.109325}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 70: {
	 	 double q[] = {0.158191, -0.047862, -0.783262, 0.66772, -0.119914, 0.065263, 0.119982, -0.0569767, -0.783262, 0.353202, 0.196606, 0.104493}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 71: {
	 	 double q[] = {0.158859, -0.05465, -0.774411, 0.67813, -0.132821, 0.0610851, 0.121895, -0.0632137, -0.774411, 0.342628, 0.204858, 0.0989663}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 72: {
	 	 double q[] = {0.155379, -0.0635614, -0.758117, 0.686916, -0.147998, 0.0570028, 0.120371, -0.0713001, -0.758117, 0.333989, 0.207288, 0.0927775}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 73: {
	 	 double q[] = {0.147422, -0.0750121, -0.733048, 0.693671, -0.165615, 0.0530695, 0.115084, -0.0816729, -0.733048, 0.32719, 0.203397, 0.0859889}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 74: {
	 	 double q[] = {0.134562, -0.08952, -0.697001, 0.697999, -0.186093, 0.0493548, 0.105607, -0.0948789, -0.697001, 0.322291, 0.192282, 0.0786801}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 75: {
	 	 double q[] = {0.115583, -0.107809, -0.647344, 0.696955, -0.208383, 0.0460294, 0.09077, -0.111724, -0.647344, 0.317203, 0.174087, 0.0710021}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 76: {
	 	 double q[] = {0.0886567, -0.130855, -0.579356, 0.686777, -0.231422, 0.0433755, 0.068809, -0.133281, -0.579356, 0.309542, 0.148426, 0.0631994}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 77: {
	 	 double q[] = {0.0499406, -0.16016, -0.4858, 0.65824, -0.251137, 0.0419612, 0.0360091, -0.161229, -0.4858, 0.292752, 0.11658, 0.0557547}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 78: {
	 	 double q[] = {-0.00807087, -0.198889, -0.354243, 0.590135, -0.256451, 0.0426964, -0.0150608, -0.199025, -0.354243, 0.251757, 0.0833088, 0.0495498}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 79: {
	 	 double q[] = {0.00783168, -0.196933, -0.364599, 0.589077, -0.26059, 0.0311666, 0.000837529, -0.197176, -0.364599, 0.249077, 0.080779, 0.0380297}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 80: {
	 	 double q[] = {0.0240334, -0.194753, -0.37333, 0.58609, -0.264466, 0.019228, 0.0170043, -0.195105, -0.37333, 0.244668, 0.0783178, 0.0261329}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 81: {
	 	 double q[] = {0.0404244, -0.192663, -0.379856, 0.581322, -0.26821, 0.00696403, 0.0333086, -0.193125, -0.379856, 0.238972, 0.0755043, 0.0139631}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 82: {
	 	 double q[] = {0.0568946, -0.19076, -0.384001, 0.574776, -0.271848, -0.00551093, 0.0496368, -0.191334, -0.384001, 0.232056, 0.0722492, 0.00163795}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 83: {
	 	 double q[] = {0.073307, -0.189157, -0.385619, 0.566537, -0.275443, -0.0180431, 0.0658484, -0.189846, -0.385619, 0.224067, 0.0684308, -0.0106857}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 84: {
	 	 double q[] = {0.0895756, -0.187973, -0.384641, 0.556677, -0.279033, -0.0305241, 0.0818535, -0.188783, -0.384641, 0.215139, 0.063951, -0.0228955}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 85: {
	 	 double q[] = {0.0740923, -0.16005, -0.456031, 0.593325, -0.282892, -0.040137, 0.0623828, -0.161734, -0.456031, 0.242661, 0.0696475, -0.0284567}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 86: {
	 	 double q[] = {0.0473908, -0.132042, -0.52476, 0.615178, -0.267546, -0.0465998, 0.0319329, -0.134768, -0.52476, 0.278352, 0.0713361, -0.0310386}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 87: {
	 	 double q[] = {0.0135919, -0.107996, -0.588638, 0.621099, -0.235397, -0.0508283, -0.00564423, -0.11191, -0.588638, 0.318688, 0.0691246, -0.031312}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 88: {
	 	 double q[] = {-0.0223296, -0.0891717, -0.645008, 0.61212, -0.190735, -0.0538824, -0.045411, -0.0943736, -0.645008, 0.360101, 0.0633986, -0.0303169}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 89: {
	 	 double q[] = {-0.0562749, -0.0745489, -0.692019, 0.591696, -0.138869, -0.0564723, -0.0831061, -0.081043, -0.692019, 0.400345, 0.0545674, -0.0289454}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 90: {
	 	 double q[] = {-0.0855898, -0.062553, -0.729521, 0.563821, -0.0844631, -0.0590148, -0.115846, -0.0702352, -0.729521, 0.438356, 0.0430103, -0.027863}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 91: {
	 	 double q[] = {-0.109193, -0.0516431, -0.758163, 0.532307, -0.0312338, -0.0615803, -0.142347, -0.0603138, -0.758163, 0.47385, 0.0290779, -0.0273617}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 92: {
	 	 double q[] = {-0.127063, -0.0407867, -0.778891, 0.500056, 0.0182734, -0.0640457, -0.162459, -0.0501849, -0.778891, 0.506857, 0.0130825, -0.0274587}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 93: {
	 	 double q[] = {-0.139693, -0.0294227, -0.792558, 0.469009, 0.0624835, -0.0662278, -0.176621, -0.039259, -0.792558, 0.537474, -0.00471357, -0.0280326}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 94: {
	 	 double q[] = {-0.147736, -0.0173009, -0.799748, 0.440319, 0.100498, -0.0679324, -0.185481, -0.0272804, -0.799748, 0.565777, -0.0241177, -0.0289001}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 95: {
	 	 double q[] = {-0.151865, -0.00419471, -0.800729, 0.414716, 0.131734, -0.0689673, -0.189726, -0.0140287, -0.800729, 0.591777, -0.0449821, -0.0298519}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 96: {
	 	 double q[] = {-0.152337, 0.00972879, -0.795457, 0.392061, 0.156228, -0.0692835, -0.189627, 0.000314665, -0.795457, 0.615327, -0.0672254, -0.0308237}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 97: {
	 	 double q[] = {-0.149513, 0.0247334, -0.783519, 0.372658, 0.173581, -0.0687669, -0.185567, 0.0160011, -0.783519, 0.636419, -0.0909133, -0.0316779}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 98: {
	 	 double q[] = {-0.143469, 0.041023, -0.764148, 0.35637, 0.183585, -0.0673729, -0.17763, 0.0332152, -0.764148, 0.654772, -0.116085, -0.032354}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 99: {
	 	 double q[] = {-0.13405, 0.0589245, -0.736119, 0.342881, 0.185952, -0.065107, -0.16566, 0.0522574, -0.736119, 0.669831, -0.142754, -0.0328494}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 100: {
	 	 double q[] = {-0.120902, 0.0789022, -0.697346, 0.332044, 0.179963, -0.0620204, -0.149292, 0.0735627, -0.697346, 0.681021, -0.171176, -0.0332134}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 101: {
	 	 double q[] = {-0.102896, 0.10173, -0.645485, 0.321647, 0.165915, -0.0582209, -0.127339, 0.0978264, -0.645485, 0.685153, -0.200026, -0.0335882}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 102: {
	 	 double q[] = {-0.0782067, 0.12836, -0.576247, 0.309375, 0.143455, -0.0539624, -0.0978895, 0.125914, -0.576247, 0.678384, -0.228049, -0.0342858}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 103: {
	 	 double q[] = {-0.0431593, 0.160177, -0.483265, 0.289264, 0.113614, -0.0497136, -0.05711, 0.159052, -0.483265, 0.651797, -0.251137, -0.0358944}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 104: {
	 	 double q[] = {0.00911018, 0.19974, -0.35574, 0.248153, 0.0800974, -0.0462471, 0.00203869, 0.199545, -0.35574, 0.585877, -0.259029, -0.0393133}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 105: {
	 	 double q[] = {-0.00547759, 0.198237, -0.364497, 0.243766, 0.0755675, -0.0370896, -0.0125614, 0.197918, -0.364497, 0.582721, -0.264782, -0.0301369}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 106: {
	 	 double q[] = {-0.0205571, 0.196736, -0.37111, 0.238528, 0.0707463, -0.0272114, -0.0277098, 0.196296, -0.37111, 0.578283, -0.270405, -0.0201827}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 107: {
	 	 double q[] = {-0.0358226, 0.195303, -0.375794, 0.231686, 0.0660987, -0.0168487, -0.0430997, 0.19474, -0.375794, 0.571747, -0.275372, -0.00968736}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 108: {
	 	 double q[] = {-0.0512839, 0.19407, -0.378077, 0.223859, 0.0611126, -0.00605709, -0.0587483, 0.193381, -0.378077, 0.563649, -0.280115, 0.00130007}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 109: {
	 	 double q[] = {-0.0667674, 0.193175, -0.377844, 0.215106, 0.0557268, 0.00498893, -0.0744861, 0.192356, -0.377844, 0.553969, -0.284614, 0.0126088}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 110: {
	 	 double q[] = {-0.0821773, 0.192743, -0.375013, 0.205542, 0.0498395, 0.0161699, -0.0902209, 0.191788, -0.375013, 0.54276, -0.288916, 0.0241229}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 111: {
	 	 double q[] = {-0.0620571, 0.164527, -0.449764, 0.233936, 0.0559412, 0.0208151, -0.0738798, 0.162635, -0.449764, 0.581811, -0.293859, 0.0326325}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 112: {
	 	 double q[] = {-0.0300176, 0.136913, -0.522839, 0.270485, 0.0586383, 0.0222774, -0.0453994, 0.133907, -0.522839, 0.605902, -0.278855, 0.0378121}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 113: {
	 	 double q[] = {0.0100019, 0.114529, -0.591308, 0.311103, 0.0580218, 0.0211946, -0.00903805, 0.110234, -0.591308, 0.612822, -0.245833, 0.0405959}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 114: {
	 	 double q[] = {0.0525655, 0.0985494, -0.651939, 0.352043, 0.0543189, 0.0187352, 0.0297239, 0.092824, -0.651939, 0.603399, -0.199219, 0.042182}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 115: {
	 	 double q[] = {0.0929238, 0.0874071, -0.702346, 0.391342, 0.0476002, 0.0159189, 0.0663231, 0.0802268, -0.702346, 0.581588, -0.144873, 0.0433815}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 116: {
	 	 double q[] = {0.12793, 0.0789348, -0.742426, 0.428186, 0.0381183, 0.0134946, 0.0978767, 0.0704056, -0.742426, 0.55186, -0.087798, 0.0446546}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 117: {
	 	 double q[] = {0.156226, 0.0711421, -0.772967, 0.462546, 0.0261237, 0.0117938, 0.123257, 0.0614895, -0.772967, 0.518443, -0.0319579, 0.0460769}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 118: {
	 	 double q[] = {0.177691, 0.06275, -0.795087, 0.494613, 0.0118869, 0.0108442, 0.142488, 0.0522762, -0.795087, 0.484493, 0.0199828, 0.0475172}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 119: {
	 	 double q[] = {0.192836, 0.0531044, -0.809783, 0.52457, -0.00434042, 0.0105237, 0.156133, 0.0421459, -0.809783, 0.45208, 0.0664016, 0.0487873}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 120: {
	 	 double q[] = {0.202381, 0.0419548, -0.817738, 0.552529, -0.0223654, 0.010642, 0.164917, 0.0308522, -0.817738, 0.422418, 0.106383, 0.0496927}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 121: {
	 	 double q[] = {0.207096, 0.0291167, -0.819297, 0.578534, -0.0420502, 0.0109822, 0.16958, 0.0181981, -0.819297, 0.396281, 0.139316, 0.0500447}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 122: {
	 	 double q[] = {0.207292, 0.0148306, -0.814438, 0.602376, -0.0632762, 0.0114711, 0.170409, 0.00440266, -0.814438, 0.373463, 0.165281, 0.0497984}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 123: {
	 	 double q[] = {0.203426, -0.00111424, -0.802791, 0.62409, -0.0861373, 0.0119627, 0.167828, -0.0107655, -0.802791, 0.354329, 0.183836, 0.0488447}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 124: {
	 	 double q[] = {0.195641, -0.0188581, -0.783608, 0.643404, -0.110683, 0.0123862, 0.161963, -0.0274739, -0.783608, 0.338753, 0.194748, 0.04714}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 125: {
	 	 double q[] = {0.183929, -0.038645, -0.755528, 0.660346, -0.137339, 0.0127366, 0.15279, -0.0459911, -0.755528, 0.32696, 0.197364, 0.0447028}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 126: {
	 	 double q[] = {0.167774, -0.060991, -0.71693, 0.672827, -0.16531, 0.0130136, 0.139824, -0.0668935, -0.71693, 0.317488, 0.191814, 0.0415247}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 127: {
	 	 double q[] = {0.146284, -0.0865245, -0.664919, 0.679249, -0.194452, 0.0133678, 0.122209, -0.090865, -0.664919, 0.309566, 0.177364, 0.0377377}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 128: {
	 	 double q[] = {0.117544, -0.116244, -0.595186, 0.675443, -0.223343, 0.0140827, 0.0981291, -0.119, -0.595186, 0.300678, 0.1537, 0.0335596}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 129: {
	 	 double q[] = {0.0776399, -0.151577, -0.500868, 0.652482, -0.24806, 0.015757, 0.0638414, -0.152882, -0.500868, 0.284979, 0.121536, 0.0294582}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 130: {
	 	 double q[] = {0.0189882, -0.194999, -0.370028, 0.590203, -0.258298, 0.019389, 0.0119679, -0.195257, -0.370028, 0.248837, 0.0844296, 0.0262809}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 131: {
	 	 double q[] = {0.0320107, -0.193508, -0.376418, 0.586067, -0.262958, 0.0109013, 0.0249432, -0.193874, -0.376418, 0.243921, 0.0805493, 0.0178462}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 132: {
	 	 double q[] = {0.0455777, -0.191904, -0.381359, 0.580729, -0.267541, 0.00177969, 0.038428, -0.192379, -0.381359, 0.238033, 0.0765214, 0.00881384}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 133: {
	 	 double q[] = {0.0594826, -0.190443, -0.384431, 0.574015, -0.271887, -0.00786605, 0.0521999, -0.191027, -0.384431, 0.23123, 0.072278, -0.00069153}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 134: {
	 	 double q[] = {0.0736295, -0.189202, -0.385489, 0.565953, -0.276051, -0.017925, 0.0661602, -0.1899, -0.385489, 0.22359, 0.0677196, -0.0105563}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 135: {
	 	 double q[] = {0.0878755, -0.188282, -0.384425, 0.556587, -0.280071, -0.0282392, 0.0801632, -0.189097, -0.384425, 0.215209, 0.0627545, -0.02062}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 136: {
	 	 double q[] = {0.102125, -0.187786, -0.381183, 0.545952, -0.283972, -0.0386957, 0.0941103, -0.188723, -0.381183, 0.206173, 0.0573072, -0.0307667}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 137: {
	 	 double q[] = {0.0845872, -0.159679, -0.452229, 0.582856, -0.288045, -0.0464122, 0.0726733, -0.161535, -0.452229, 0.233709, 0.0630072, -0.0345062}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 138: {
	 	 double q[] = {0.0557395, -0.131403, -0.521456, 0.60592, -0.273155, -0.0510725, 0.0402029, -0.134327, -0.521456, 0.269999, 0.0648243, -0.0353976}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 139: {
	 	 double q[] = {0.0196775, -0.107386, -0.586287, 0.61306, -0.241156, -0.0536114, 0.000474296, -0.111521, -0.586287, 0.311027, 0.0629754, -0.0340803}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 140: {
	 	 double q[] = {-0.0184807, -0.0889019, -0.643663, 0.605099, -0.196341, -0.0551325, -0.0414543, -0.0943518, -0.643663, 0.353118, 0.0577421, -0.0316151}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 141: {
	 	 double q[] = {-0.0545084, -0.0748474, -0.691558, 0.585379, -0.144018, -0.0563756, -0.0811983, -0.081618, -0.691558, 0.393929, 0.0495179, -0.0289195}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 142: {
	 	 double q[] = {-0.085645, -0.0635144, -0.729742, 0.557969, -0.0889394, -0.0577718, -0.11576, -0.0714982, -0.729742, 0.432435, 0.0386263, -0.0266831}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 143: {
	 	 double q[] = {-0.110754, -0.0532413, -0.758869, 0.526776, -0.0349017, -0.0593904, -0.143784, -0.0622278, -0.758869, 0.468393, 0.0253872, -0.0252133}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 144: {
	 	 double q[] = {-0.129796, -0.0429128, -0.779922, 0.494794, 0.0154815, -0.0610996, -0.165091, -0.0526254, -0.779922, 0.501869, 0.0100916, -0.0245319}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 145: {
	 	 double q[] = {-0.143272, -0.0319227, -0.793789, 0.464035, 0.0605798, -0.0627043, -0.180119, -0.0420549, -0.793789, 0.532994, -0.00701667, -0.024514}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 146: {
	 	 double q[] = {-0.15186, -0.0200053, -0.801084, 0.435706, 0.0994512, -0.0639978, -0.189535, -0.0302462, -0.801084, 0.561864, -0.0257605, -0.0249666}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 147: {
	 	 double q[] = {-0.156261, -0.00694016, -0.802101, 0.410544, 0.131493, -0.0647755, -0.19406, -0.0169893, -0.802101, 0.588481, -0.0459912, -0.0256669}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 148: {
	 	 double q[] = {-0.156772, 0.00708914, -0.796795, 0.388468, 0.156694, -0.0649761, -0.194002, -0.00248506, -0.796795, 0.612739, -0.0676628, -0.0265342}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 149: {
	 	 double q[] = {-0.153786, 0.022321, -0.784765, 0.36976, 0.174655, -0.0644737, -0.189782, 0.0134868, -0.784765, 0.634603, -0.090833, -0.0274149}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 150: {
	 	 double q[] = {-0.14741, 0.038932, -0.765244, 0.35427, 0.185158, -0.0632116, -0.181516, 0.0310792, -0.765244, 0.653775, -0.115541, -0.0282334}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 151: {
	 	 double q[] = {-0.137585, 0.0571828, -0.736823, 0.342104, 0.187604, -0.0611967, -0.169153, 0.0505306, -0.736823, 0.670173, -0.142164, -0.0289803}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 152: {
	 	 double q[] = {-0.12377, 0.0776331, -0.697934, 0.331785, 0.182184, -0.0584296, -0.152122, 0.0723411, -0.697934, 0.681707, -0.169862, -0.0296668}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 153: {
	 	 double q[] = {-0.105031, 0.100902, -0.645698, 0.322307, 0.168332, -0.0550496, -0.129447, 0.0970739, -0.645698, 0.686576, -0.19835, -0.0304533}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 154: {
	 	 double q[] = {-0.0794484, 0.127943, -0.575984, 0.310841, 0.146041, -0.0512952, -0.0991179, 0.125583, -0.575984, 0.680361, -0.225966, -0.0316414}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 155: {
	 	 double q[] = {-0.0432883, 0.160049, -0.482452, 0.291219, 0.11649, -0.0476391, -0.0572407, 0.159002, -0.482452, 0.653968, -0.248476, -0.0338242}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 156: {
	 	 double q[] = {0.0103469, 0.199588, -0.354435, 0.249978, 0.0836634, -0.0448545, 0.00325542, 0.199439, -0.354435, 0.587669, -0.255433, -0.0379021}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 157: {
	 	 double q[] = {-0.00336137, 0.198087, -0.363228, 0.246217, 0.0795602, -0.0362505, -0.0104467, 0.197822, -0.363228, 0.585245, -0.260862, -0.0292984}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 158: {
	 	 double q[] = {-0.0176057, 0.196573, -0.370189, 0.241671, 0.0751875, -0.0269329, -0.0247345, 0.196194, -0.370189, 0.581666, -0.266198, -0.0199309}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 159: {
	 	 double q[] = {-0.0320864, 0.195083, -0.375519, 0.235589, 0.0710112, -0.0171284, -0.0393069, 0.194589, -0.375519, 0.576136, -0.270934, -0.0100273}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 160: {
	 	 double q[] = {-0.0468081, 0.193743, -0.378736, 0.228545, 0.0665366, -0.00689104, -0.0541759, 0.193131, -0.378736, 0.569141, -0.275475, 0.000365156}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 161: {
	 	 double q[] = {-0.0615996, 0.19267, -0.379702, 0.220582, 0.0617037, 0.00361055, -0.069174, 0.191938, -0.379702, 0.560648, -0.279809, 0.0110814}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 162: {
	 	 double q[] = {-0.0763673, 0.19197, -0.378316, 0.211804, 0.0564103, 0.0142613, -0.0842112, 0.191113, -0.378316, 0.550707, -0.283986, 0.0220093}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 163: {
	 	 double q[] = {-0.055678, 0.16403, -0.453427, 0.241024, 0.0625099, 0.0183391, -0.0673501, 0.162279, -0.453427, 0.59041, -0.288772, 0.0299884}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 164: {
	 	 double q[] = {-0.0232487, 0.136933, -0.526345, 0.278057, 0.065117, 0.0192525, -0.0385672, 0.134105, -0.526345, 0.61446, -0.273355, 0.0346918}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 165: {
	 	 double q[] = {0.0169414, 0.115001, -0.59431, 0.319047, 0.064282, 0.0176555, -0.00213162, 0.11092, -0.59431, 0.621161, -0.239981, 0.0370412}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 166: {
	 	 double q[] = {0.0595213, 0.0993706, -0.654324, 0.360231, 0.0603406, 0.014713, 0.0365569, 0.0939012, -0.654324, 0.611376, -0.193021, 0.0382155}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 167: {
	 	 double q[] = {0.0997754, 0.0884528, -0.704085, 0.399726, 0.0533526, 0.0114475, 0.0729816, 0.0815745, -0.704085, 0.5892, -0.138396, 0.0390163}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 168: {
	 	 double q[] = {0.134599, 0.0801181, -0.743571, 0.436691, 0.0436117, 0.00860593, 0.104308, 0.0719396, -0.743571, 0.559089, -0.0810871, 0.0398971}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 169: {
	 	 double q[] = {0.162657, 0.0724107, -0.773594, 0.471092, 0.0313792, 0.00651977, 0.129434, 0.0631561, -0.773594, 0.525276, -0.0250546, 0.0409336}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 170: {
	 	 double q[] = {0.183854, 0.064078, -0.795276, 0.503117, 0.016929, 0.00521524, 0.148405, 0.0540437, -0.795276, 0.490918, 0.0270351, 0.0419972}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 171: {
	 	 double q[] = {0.19872, 0.0544824, -0.809609, 0.532949, 0.00051266, 0.00456863, 0.161802, 0.0439955, -0.809609, 0.458087, 0.0735567, 0.0429042}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 172: {
	 	 double q[] = {0.207996, 0.0433822, -0.817271, 0.560702, -0.0176778, 0.0043876, 0.17036, 0.0327711, -0.817271, 0.428, 0.113591, 0.0434646}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 173: {
	 	 double q[] = {0.212374, 0.0307104, -0.818585, 0.586416, -0.0375201, 0.00449323, 0.17474, 0.0202901, -0.818585, 0.40129, 0.146646, 0.0435315}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 174: {
	 	 double q[] = {0.212462, 0.0163487, -0.813557, 0.609898, -0.0588504, 0.00469212, 0.175512, 0.0064109, -0.813557, 0.378191, 0.172436, 0.042953}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 175: {
	 	 double q[] = {0.208427, 0.000447143, -0.801775, 0.631156, -0.0817981, 0.00496072, 0.172814, -0.00873639, -0.801775, 0.358608, 0.190898, 0.041739}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 176: {
	 	 double q[] = {0.200518, -0.0172734, -0.782501, 0.649929, -0.106399, 0.00518781, 0.166868, -0.0254577, -0.782501, 0.342579, 0.201671, 0.0398118}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 177: {
	 	 double q[] = {0.188727, -0.0370757, -0.754369, 0.666251, -0.133077, 0.00536838, 0.157647, -0.044039, -0.754369, 0.330343, 0.204092, 0.0371935}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 178: {
	 	 double q[] = {0.172518, -0.0594928, -0.715774, 0.678016, -0.161014, 0.00550884, 0.144643, -0.0650721, -0.715774, 0.320428, 0.198309, 0.0338835}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 179: {
	 	 double q[] = {0.150981, -0.0851716, -0.663838, 0.683629, -0.190054, 0.00576388, 0.12698, -0.0892567, -0.663838, 0.312071, 0.183595, 0.0300196}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 180: {
	 	 double q[] = {0.122174, -0.115125, -0.594294, 0.678931, -0.218757, 0.00642457, 0.102811, -0.117698, -0.594294, 0.302768, 0.159655, 0.0258276}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 181: {
	 	 double q[] = {0.082152, -0.150765, -0.500367, 0.655042, -0.243189, 0.0080977, 0.0683628, -0.15196, -0.500367, 0.286703, 0.127229, 0.0217815}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 182: {
	 	 double q[] = {0.0233663, -0.194466, -0.370277, 0.592018, -0.253146, 0.0117817, 0.0162985, -0.194679, -0.370277, 0.250353, 0.0898858, 0.0187195}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 183: {
	 	 double q[] = {0.0366855, -0.192817, -0.377021, 0.587819, -0.258108, 0.00322825, 0.0295809, -0.193143, -0.377021, 0.245237, 0.0858363, 0.0102085}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 184: {
	 	 double q[] = {0.0505017, -0.191097, -0.38214, 0.582317, -0.262997, -0.00592032, 0.0433217, -0.191536, -0.38214, 0.239101, 0.0815848, 0.00114239}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 185: {
	 	 double q[] = {0.0646033, -0.189562, -0.385235, 0.575336, -0.267633, -0.0155546, 0.0572934, -0.190115, -0.385235, 0.231995, 0.0770873, -0.00835475}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 186: {
	 	 double q[] = {0.0789013, -0.188292, -0.386175, 0.566926, -0.272076, -0.0255659, 0.0714046, -0.188961, -0.386175, 0.224017, 0.0722393, -0.0181718}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 187: {
	 	 double q[] = {0.0932582, -0.187386, -0.384876, 0.557154, -0.276368, -0.0357992, 0.085515, -0.188175, -0.384876, 0.215281, 0.0669501, -0.0281513}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 188: {
	 	 double q[] = {0.107583, -0.186949, -0.381298, 0.546063, -0.280535, -0.0461442, 0.0995302, -0.187863, -0.381298, 0.205883, 0.0611461, -0.0381798}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 189: {
	 	 double q[] = {0.0902965, -0.158586, -0.451904, 0.58263, -0.284934, -0.0538136, 0.0783162, -0.160414, -0.451904, 0.233166, 0.0664317, -0.0418451}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 190: {
	 	 double q[] = {0.0617776, -0.129957, -0.520667, 0.605628, -0.270568, -0.0584107, 0.0461657, -0.132847, -0.520667, 0.269354, 0.0677507, -0.042666}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 191: {
	 	 double q[] = {0.0260257, -0.105515, -0.585166, 0.612944, -0.239214, -0.0608474, 0.00675908, -0.109606, -0.585166, 0.310423, 0.065376, -0.0412601}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 192: {
	 	 double q[] = {-0.0119257, -0.0865849, -0.642366, 0.605331, -0.195102, -0.062213, -0.0349364, -0.0919784, -0.642366, 0.352656, 0.0596249, -0.038668}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 193: {
	 	 double q[] = {-0.047893, -0.0721458, -0.690222, 0.58602, -0.143465, -0.0632468, -0.0745904, -0.0788491, -0.690222, 0.393653, 0.0509154, -0.0357945}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 194: {
	 	 double q[] = {-0.0791182, -0.060543, -0.72845, 0.558988, -0.0890068, -0.0643876, -0.109217, -0.0684535, -0.72845, 0.432338, 0.0395832, -0.0333273}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 195: {
	 	 double q[] = {-0.104443, -0.0501325, -0.75766, 0.528094, -0.0355018, -0.0657154, -0.137445, -0.0590466, -0.75766, 0.468442, 0.0259512, -0.0315786}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 196: {
	 	 double q[] = {-0.123799, -0.0397919, -0.778806, 0.496313, 0.0144418, -0.0671071, -0.159066, -0.0494399, -0.778806, 0.502017, 0.0103103, -0.0305786}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 197: {
	 	 double q[] = {-0.137659, -0.0288964, -0.79276, 0.465652, 0.0591925, -0.068374, -0.174487, -0.0389769, -0.79276, 0.53319, -0.00709581, -0.0302119}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 198: {
	 	 double q[] = {-0.146675, -0.0171575, -0.800135, 0.437323, 0.0978036, -0.0693139, -0.184346, -0.0273629, -0.800135, 0.562056, -0.0260914, -0.0302939}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 199: {
	 	 double q[] = {-0.151529, -0.0043367, -0.801223, 0.412071, 0.129669, -0.069726, -0.189338, -0.0143675, -0.801223, 0.588625, -0.0465318, -0.0306105}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 200: {
	 	 double q[] = {-0.152507, 0.0094127, -0.795986, 0.389837, 0.154759, -0.0695492, -0.189763, -0.000159843, -0.795986, 0.612796, -0.0683719, -0.0310841}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 201: {
	 	 double q[] = {-0.149993, 0.0243392, -0.784024, 0.370911, 0.172672, -0.0686604, -0.186023, 0.0154921, -0.784024, 0.63454, -0.0916744, -0.0315665}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 202: {
	 	 double q[] = {-0.144085, 0.0406324, -0.764575, 0.355156, 0.183183, -0.0670045, -0.17823, 0.0327549, -0.764575, 0.653569, -0.116484, -0.0319849}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 203: {
	 	 double q[] = {-0.134654, 0.0586018, -0.736412, 0.342248, 0.185993, -0.0645766, -0.166253, 0.0519078, -0.736412, 0.66931, -0.142814, -0.0323231}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 204: {
	 	 double q[] = {-0.121366, 0.0786954, -0.697437, 0.332038, 0.180366, -0.0614176, -0.14975, 0.0733661, -0.697437, 0.681177, -0.170929, -0.0326184}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 205: {
	 	 double q[] = {-0.10309, 0.101662, -0.64531, 0.322232, 0.166638, -0.0576279, -0.12753, 0.097796, -0.64531, 0.685897, -0.199461, -0.0330044}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 206: {
	 	 double q[] = {-0.0779912, 0.128427, -0.575729, 0.310458, 0.144482, -0.0534572, -0.097673, 0.126032, -0.575729, 0.679566, -0.227123, -0.0337883}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 207: {
	 	 double q[] = {-0.0423585, 0.160314, -0.482348, 0.290608, 0.115029, -0.0493749, -0.0563116, 0.15924, -0.482348, 0.653133, -0.249716, -0.0355577}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 208: {
	 	 double q[] = {0.0106949, 0.199761, -0.354466, 0.249301, 0.0821835, -0.0461535, 0.0036124, 0.199596, -0.354466, 0.586913, -0.256833, -0.0392098}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 209: {
	 	 double q[] = {-0.00345975, 0.19823, -0.363376, 0.245417, 0.0780463, -0.0371687, -0.0105417, 0.197948, -0.363376, 0.584363, -0.262293, -0.0302194}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 210: {
	 	 double q[] = {-0.018131, 0.196687, -0.370354, 0.240704, 0.0736351, -0.0274753, -0.0252644, 0.19629, -0.370354, 0.580591, -0.267645, -0.0204679}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 211: {
	 	 double q[] = {-0.033024, 0.195177, -0.375593, 0.23441, 0.0694092, -0.0172979, -0.0402591, 0.194662, -0.375593, 0.574803, -0.272386, -0.0101812}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 212: {
	 	 double q[] = {-0.0481467, 0.19383, -0.378607, 0.227125, 0.0648619, -0.00669053, -0.0555417, 0.193195, -0.378607, 0.567494, -0.276929, 0.000594242}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 213: {
	 	 double q[] = {-0.0633292, 0.192773, -0.379259, 0.218895, 0.0599298, 0.00417815, -0.0709464, 0.192014, -0.379259, 0.558631, -0.281263, 0.0116932}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 214: {
	 	 double q[] = {-0.0784792, 0.192122, -0.377446, 0.209826, 0.0545077, 0.0151931, -0.0863844, 0.191235, -0.377446, 0.548262, -0.285434, 0.0230042}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 215: {
	 	 double q[] = {-0.0581626, 0.164061, -0.452416, 0.238708, 0.0606504, 0.0196576, -0.069885, 0.162265, -0.452416, 0.587675, -0.29022, 0.0313628}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 216: {
	 	 double q[] = {-0.0260788, 0.136743, -0.525334, 0.275499, 0.0633129, 0.0209653, -0.0414232, 0.133857, -0.525334, 0.611649, -0.274907, 0.0364408}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 217: {
	 	 double q[] = {0.0138019, 0.114573, -0.593401, 0.316291, 0.0625526, 0.0197671, -0.00526614, 0.110423, -0.593401, 0.618364, -0.241661, 0.0391638}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 218: {
	 	 double q[] = {0.0560996, 0.0986952, -0.653561, 0.357339, 0.0586712, 0.0172251, 0.0331732, 0.0931429, -0.653561, 0.608693, -0.194879, 0.0407118}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 219: {
	 	 double q[] = {0.0961064, 0.0875405, -0.703496, 0.396736, 0.0517362, 0.0143537, 0.0693803, 0.0805653, -0.703496, 0.586679, -0.14045, 0.0418837}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 220: {
	 	 double q[] = {0.130721, 0.0789828, -0.743159, 0.433651, 0.0420302, 0.0118936, 0.10052, 0.0706922, -0.743159, 0.556779, -0.0833561, 0.0431307}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 221: {
	 	 double q[] = {0.158615, 0.0710717, -0.773348, 0.468048, 0.0298135, 0.0101689, 0.125493, 0.0616895, -0.773348, 0.523211, -0.0275463, 0.0445237}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 222: {
	 	 double q[] = {0.179694, 0.0625593, -0.795177, 0.500115, 0.0153616, 0.00919995, 0.144345, 0.0523828, -0.795177, 0.489122, 0.0243241, 0.0459289}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 223: {
	 	 double q[] = {0.194486, 0.0528131, -0.809634, 0.530034, -0.00107049, 0.00885692, 0.157654, 0.0421711, -0.809634, 0.456573, 0.0706419, 0.0471552}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 224: {
	 	 double q[] = {0.203726, 0.0415947, -0.817394, 0.557917, -0.0192864, 0.00894263, 0.166155, 0.0308193, -0.817394, 0.42677, 0.1105, 0.0480049}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 225: {
	 	 double q[] = {0.208196, 0.0287212, -0.818789, 0.583811, -0.0391464, 0.00923175, 0.170597, 0.0181318, -0.818789, 0.40049, 0.143294, 0.048284}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 226: {
	 	 double q[] = {0.208206, 0.0144434, -0.813793, 0.607495, -0.0605185, 0.00964711, 0.171264, 0.00433816, -0.813793, 0.377502, 0.169127, 0.0479449}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 227: {
	 	 double q[] = {0.204218, -0.00145849, -0.802025, 0.629014, -0.0834952, 0.0100337, 0.168585, -0.0108013, -0.802025, 0.358179, 0.187558, 0.0468707}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 228: {
	 	 double q[] = {0.196376, -0.0191319, -0.782735, 0.648094, -0.108118, 0.0103143, 0.162684, -0.0274601, -0.782735, 0.342397, 0.198363, 0.0450118}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 229: {
	 	 double q[] = {0.184674, -0.0388334, -0.754549, 0.664769, -0.134807, 0.0104744, 0.153539, -0.0459185, -0.754549, 0.330391, 0.20089, 0.0423782}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 230: {
	 	 double q[] = {0.168578, -0.0610895, -0.715854, 0.676925, -0.162737, 0.0105112, 0.140644, -0.0667632, -0.715854, 0.320677, 0.195295, 0.0389592}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 231: {
	 	 double q[] = {0.147184, -0.0865454, -0.663756, 0.68295, -0.191739, 0.010568, 0.123133, -0.0906944, -0.663756, 0.312484, 0.180856, 0.0348811}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 232: {
	 	 double q[] = {0.118552, -0.116214, -0.59397, 0.678639, -0.220343, 0.0109271, 0.0991575, -0.118821, -0.59397, 0.303277, 0.157293, 0.030363}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 233: {
	 	 double q[] = {0.078724, -0.151517, -0.499704, 0.655021, -0.24455, 0.0121872, 0.0649294, -0.152723, -0.499704, 0.287179, 0.125382, 0.0258757}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 234: {
	 	 double q[] = {0.0201169, -0.194852, -0.369192, 0.591965, -0.254006, 0.0153445, 0.0130656, -0.195064, -0.369192, 0.250546, 0.0887782, 0.0222655}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 235: {
	 	 double q[] = {0.0337422, -0.193187, -0.376117, 0.588142, -0.258437, 0.00614799, 0.0266535, -0.193505, -0.376117, 0.245758, 0.0853093, 0.0131118}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 236: {
	 	 double q[] = {0.047958, -0.191403, -0.381495, 0.583096, -0.262694, -0.00380057, 0.0407961, -0.191828, -0.381495, 0.23998, 0.0817859, 0.00324316}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 237: {
	 	 double q[] = {0.0625556, -0.189758, -0.384927, 0.576642, -0.266585, -0.0144005, 0.0552691, -0.19029, -0.384927, 0.23326, 0.0781726, -0.00722548}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 238: {
	 	 double q[] = {0.0774467, -0.188332, -0.38627, 0.568826, -0.27016, -0.0255525, 0.0699824, -0.188971, -0.38627, 0.225686, 0.0743797, -0.0181929}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 239: {
	 	 double q[] = {0.0924948, -0.187228, -0.385431, 0.559706, -0.273446, -0.0371118, 0.0847974, -0.187974, -0.385431, 0.217362, 0.0703333, -0.0295127}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 240: {
	 	 double q[] = {0.10761, -0.186551, -0.382366, 0.549324, -0.276454, -0.0489763, 0.0996214, -0.187408, -0.382366, 0.208381, 0.0659748, -0.0410804}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 241: {
	 	 double q[] = {0.0938483, -0.160787, -0.450971, 0.586348, -0.28292, -0.0592235, 0.0816544, -0.162564, -0.450971, 0.231655, 0.073736, -0.0470582}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 242: {
	 	 double q[] = {0.0734031, -0.138027, -0.51432, 0.610947, -0.277337, -0.0678675, 0.0571088, -0.140956, -0.51432, 0.255934, 0.0799418, -0.0514677}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 243: {
	 	 double q[] = {0.048555, -0.119652, -0.57209, 0.623918, -0.261593, -0.075217, 0.0283104, -0.123905, -0.57209, 0.280271, 0.0845136, -0.0546773}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 244: {
	 	 double q[] = {0.0219226, -0.105624, -0.623352, 0.627207, -0.238695, -0.0816801, -0.0020532, -0.111294, -0.623352, 0.303734, 0.0873729, -0.0571801}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 245: {
	 	 double q[] = {-0.00419829, -0.0952872, -0.667552, 0.622942, -0.211469, -0.0876259, -0.0316074, -0.10239, -0.667552, 0.325571, 0.0886069, -0.0594411}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 246: {
	 	 double q[] = {-0.0280804, -0.0877357, -0.70469, 0.613018, -0.182168, -0.0933288, -0.0585378, -0.0962098, -0.70469, 0.345178, 0.0884685, -0.0618386}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 247: {
	 	 double q[] = {-0.0485932, -0.0817122, -0.734746, 0.60016, -0.153275, -0.0988767, -0.0816194, -0.091404, -0.734746, 0.36301, 0.0867303, -0.0645767}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 248: {
	 	 double q[] = {-0.0650746, -0.0762453, -0.758132, 0.58622, -0.126365, -0.104314, -0.100111, -0.086928, -0.758132, 0.379351, 0.08336, -0.0677966}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 249: {
	 	 double q[] = {-0.0771491, -0.0707965, -0.77537, 0.571731, -0.101936, -0.10966, -0.113576, -0.0821996, -0.77537, 0.393867, 0.0787117, -0.0715928}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 250: {
	 	 double q[] = {-0.0846139, -0.0646936, -0.786596, 0.558017, -0.0809701, -0.114851, -0.121756, -0.0764911, -0.786596, 0.407121, 0.0725468, -0.0759684}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 251: {
	 	 double q[] = {-0.0873083, -0.0575308, -0.791835, 0.545725, -0.0639616, -0.11984, -0.124453, -0.0693616, -0.791835, 0.419381, 0.0647383, -0.0809282}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 252: {
	 	 double q[] = {-0.0850591, -0.0490487, -0.790861, 0.535256, -0.0512814, -0.12459, -0.121465, -0.0605299, -0.790861, 0.430879, 0.0550896, -0.0864693}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 253: {
	 	 double q[] = {-0.0776295, -0.0391273, -0.783118, 0.526803, -0.0432522, -0.129088, -0.112536, -0.0498695, -0.783118, 0.441781, 0.0433229, -0.0925994}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 254: {
	 	 double q[] = {-0.0646974, -0.0278179, -0.767622, 0.520336, -0.0401931, -0.133343, -0.0973334, -0.0374442, -0.767622, 0.452135, 0.0290731, -0.099334}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 255: {
	 	 double q[] = {-0.0457856, -0.0154245, -0.742652, 0.51596, -0.0427411, -0.137411, -0.0753744, -0.0235853, -0.742652, 0.46225, 0.0115459, -0.106723}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 256: {
	 	 double q[] = {-0.020393, -0.00266069, -0.705916, 0.511866, -0.0506196, -0.141329, -0.0461401, -0.00908719, -0.705916, 0.470582, -0.00918464, -0.114793}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 257: {
	 	 double q[] = {0.0121797, 0.00905037, -0.65345, 0.506015, -0.0639301, -0.145185, -0.00889141, 0.00452748, -0.65345, 0.475644, -0.0337029, -0.123634}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 258: {
	 	 double q[] = {0.0525113, 0.0169737, -0.579159, 0.492938, -0.081353, -0.148981, 0.0370627, 0.0143588, -0.579159, 0.472968, -0.0616249, -0.133315}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 259: {
	 	 double q[] = {0.100475, 0.0162463, -0.473511, 0.460098, -0.0981882, -0.152456, 0.0918652, 0.015281, -0.473511, 0.451762, -0.0899874, -0.143794}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 260: {
	 	 double q[] = {0.154014, 0.000224738, -0.319774, 0.380384, -0.102927, -0.15456, 0.154044, 0.000226024, -0.319774, 0.386311, -0.108855, -0.15459}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 261: {
	 	 double q[] = {0.152618, 0.000409414, -0.322277, 0.380268, -0.103568, -0.153184, 0.152649, 0.000410832, -0.322277, 0.386437, -0.109737, -0.153215}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 262: {
	 	 double q[] = {0.150444, 0.00143015, -0.325561, 0.382508, -0.105408, -0.150979, 0.150473, 0.00143154, -0.325561, 0.38804, -0.110941, -0.151008}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 263: {
	 	 double q[] = {0.147531, 0.00203098, -0.330252, 0.384475, -0.106189, -0.148058, 0.147558, 0.0020324, -0.330252, 0.389801, -0.111514, -0.148085}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 264: {
	 	 double q[] = {0.143936, 0.00299067, -0.335653, 0.387705, -0.107379, -0.144434, 0.143961, 0.00299202, -0.335653, 0.392413, -0.112087, -0.144459}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 265: {
	 	 double q[] = {0.139713, 0.00376934, -0.341911, 0.391037, -0.108055, -0.140191, 0.139735, 0.00377061, -0.341911, 0.395271, -0.11229, -0.140213}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 266: {
	 	 double q[] = {0.134919, 0.00461133, -0.348675, 0.394902, -0.10871, -0.135372, 0.134937, 0.00461248, -0.348675, 0.398534, -0.112343, -0.13539}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 267: {
	 	 double q[] = {0.129612, 0.00534675, -0.35585, 0.398901, -0.109097, -0.130043, 0.129627, 0.00534776, -0.35585, 0.401967, -0.112163, -0.130059}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 268: {
	 	 double q[] = {0.123851, 0.00600609, -0.363233, 0.403038, -0.10932, -0.124262, 0.123863, 0.00600693, -0.363233, 0.405532, -0.111814, -0.124274}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 269: {
	 	 double q[] = {0.117696, 0.00654546, -0.37068, 0.407174, -0.10935, -0.118089, 0.117705, 0.00654612, -0.37068, 0.409128, -0.111303, -0.118099}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 270: {
	 	 double q[] = {0.111208, 0.00695533, -0.378048, 0.411239, -0.109209, -0.111584, 0.111214, 0.00695583, -0.378048, 0.412693, -0.110662, -0.111591}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 271: {
	 	 double q[] = {0.104446, 0.00722426, -0.385213, 0.415166, -0.108912, -0.104806, 0.10445, 0.0072246, -0.385213, 0.41617, -0.109916, -0.10481}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 272: {
	 	 double q[] = {0.0974672, 0.00734915, -0.39207, 0.418903, -0.108479, -0.0978119, 0.0974697, 0.00734935, -0.39207, 0.419515, -0.10909, -0.0978144}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 273: {
	 	 double q[] = {0.0903298, 0.00733221, -0.398535, 0.422413, -0.107929, -0.090658, 0.0903309, 0.0073323, -0.398535, 0.422693, -0.10821, -0.090659}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 274: {
	 	 double q[] = {0.083088, 0.0071807, -0.40454, 0.42567, -0.107287, -0.0833983, 0.083088, 0.0071807, -0.40454, 0.425681, -0.107298, -0.0833984}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 275: {
	 	 double q[] = {0.075794, 0.00690599, -0.410035, 0.428657, -0.106572, -0.0760848, 0.0757934, 0.00690593, -0.410035, 0.42846, -0.106375, -0.0760841}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 276: {
	 	 double q[] = {0.0684974, 0.0065226, -0.414987, 0.431368, -0.105806, -0.0687666, 0.0684964, 0.00652251, -0.414987, 0.431019, -0.105457, -0.0687656}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 277: {
	 	 double q[] = {0.0612447, 0.00604725, -0.419378, 0.433801, -0.105009, -0.0614904, 0.0612436, 0.00604715, -0.419378, 0.433352, -0.10456, -0.0614892}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 278: {
	 	 double q[] = {0.0540794, 0.0054979, -0.423202, 0.43596, -0.104198, -0.0542997, 0.0540782, 0.00549779, -0.423202, 0.435458, -0.103695, -0.0542986}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 279: {
	 	 double q[] = {0.0470415, 0.0048929, -0.426469, 0.437855, -0.103387, -0.0472353, 0.0470405, 0.00489281, -0.426469, 0.437338, -0.102871, -0.0472342}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 280: {
	 	 double q[] = {0.0401679, 0.00425026, -0.429195, 0.439496, -0.102592, -0.0403343, 0.0401671, 0.00425018, -0.429195, 0.438997, -0.102093, -0.0403335}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 281: {
	 	 double q[] = {0.0334923, 0.00358699, -0.431407, 0.4409, -0.101821, -0.0336311, 0.0334916, 0.00358693, -0.431407, 0.440444, -0.101365, -0.0336304}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 282: {
	 	 double q[] = {0.0270447, 0.00291862, -0.433139, 0.442081, -0.101083, -0.0271562, 0.0270442, 0.00291858, -0.433139, 0.441687, -0.100689, -0.0271558}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 283: {
	 	 double q[] = {0.0208521, 0.00225881, -0.43443, 0.443058, -0.100386, -0.0209374, 0.0208519, 0.00225879, -0.43443, 0.442738, -0.100066, -0.0209371}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 284: {
	 	 double q[] = {0.0149383, 0.00161918, -0.435323, 0.443849, -0.0997326, -0.0149985, 0.0149382, 0.00161916, -0.435323, 0.443611, -0.0994946, -0.0149984}; 
	 walk_command.assign(q, q + len);  
	break;}
	case 285: {
	 	 double q[] = {0.00932361, 0.00100913, -0.435866, 0.444472, -0.0991268, -0.00936054, 0.00932355, 0.00100913, -0.435866, 0.444319, -0.0989735, -0.00936048}; 
	 walk_command.assign(q, q + len);  
	break;}
	}
}

