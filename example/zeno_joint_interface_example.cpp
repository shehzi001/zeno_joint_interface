#include <stdio.h>
#include <string>
#include <errno.h>
#include <limits.h>
#include <assert.h>
#include <stdlib.h>
#include <vector>
#include <map>
#include "oru_robot_interface.h"
#include <zeno_joint_ids.h>


std::string connection_ip = "127.0.0.1";
const int connection_port = 19997;

int main(int argc, char *argv[])
{
    boost::shared_ptr<oru_robot_interface> robot_interface_;

    std::vector<std::string> joint_names_;
    std::vector<double> init_joint_angles;

    joint_names_.resize(JOINTS_NUM_1);    
    joint_names_[HEAD_PITCH_1]       = "neck_pitch";
    joint_names_[HEAD_YAW_1]         = "neck_yaw";

    joint_names_[L_ANKLE_PITCH_1]    = "left_ankle_pitch";
    joint_names_[L_ANKLE_ROLL_1]     = "left_ankle_roll";
    joint_names_[L_ELBOW_ROLL_1]     = "left_elbow_pitch";
    joint_names_[L_ELBOW_YAW_1]      = "left_elbow_yaw";
    joint_names_[L_HIP_PITCH_1]      = "left_hip_pitch";
    joint_names_[L_HIP_ROLL_1]       = "left_hip_roll";
    joint_names_[L_HIP_YAW_PITCH_1]  = "left_hip_yaw";
    joint_names_[L_KNEE_PITCH_1]     = "left_knee_pitch";
    joint_names_[L_SHOULDER_PITCH_1] = "left_shoulder_pitch";
    joint_names_[L_SHOULDER_ROLL_1]  = "left_shoulder_roll";
    joint_names_[L_WRIST_YAW_1]      = "left_wrist_yaw";

    joint_names_[R_ANKLE_PITCH_1]    = "right_ankle_pitch";
    joint_names_[R_ANKLE_ROLL_1]     = "right_ankle_roll";
    joint_names_[R_ELBOW_ROLL_1]     = "right_elbow_pitch";
    joint_names_[R_ELBOW_YAW_1]      = "right_elbow_yaw";
    joint_names_[R_HIP_PITCH_1]      = "right_hip_pitch";
    joint_names_[R_HIP_ROLL_1]       = "right_hip_roll";
    joint_names_[R_HIP_YAW_PITCH_1]  = "right_hip_yaw";
    joint_names_[R_KNEE_PITCH_1]     = "right_knee_pitch";
    joint_names_[R_SHOULDER_PITCH_1] = "right_shoulder_pitch";
    joint_names_[R_SHOULDER_ROLL_1]  = "right_shoulder_roll";
    joint_names_[R_WRIST_YAW_1]      = "right_wrist_yaw";

    init_joint_angles.resize(JOINTS_NUM_1);

    init_joint_angles[L_HIP_YAW_PITCH_1]  =  -0.058;
    // note, that R_HIP_YAW_PITCH is controlled by the same motor as L_HIP_YAW_PITCH 
    init_joint_angles[R_HIP_YAW_PITCH_1]  =  -0.058;

    init_joint_angles[L_HIP_ROLL_1]       = 0.427431;
    init_joint_angles[L_HIP_PITCH_1]      = 0.468272;
    init_joint_angles[L_KNEE_PITCH_1]     =  0.770214;
    init_joint_angles[L_ANKLE_PITCH_1]    = 0.32498;
    init_joint_angles[L_ANKLE_ROLL_1]     =  0.0179769;

    init_joint_angles[R_HIP_ROLL_1]       = 0.36076;
    init_joint_angles[R_HIP_PITCH_1]      = 0.504226;
    init_joint_angles[R_KNEE_PITCH_1]     =  0.739671;
    init_joint_angles[R_ANKLE_PITCH_1]    = 0.278904;
    init_joint_angles[R_ANKLE_ROLL_1]     =  -0.0844739;

    init_joint_angles[L_SHOULDER_PITCH_1] =  0.75;
    init_joint_angles[L_SHOULDER_ROLL_1]  =  -1.50;
    init_joint_angles[L_ELBOW_YAW_1]      = 1.0;
    init_joint_angles[L_ELBOW_ROLL_1]     = 1.0;
    init_joint_angles[L_WRIST_YAW_1]      = 0.0;

    init_joint_angles[R_SHOULDER_PITCH_1] =  -0.75;
    init_joint_angles[R_SHOULDER_ROLL_1]  = 1.50;
    init_joint_angles[R_ELBOW_YAW_1]      =  -1.0;
    init_joint_angles[R_ELBOW_ROLL_1]     = 1.0;
    init_joint_angles[R_WRIST_YAW_1]      = 0.0; 

    init_joint_angles[HEAD_PITCH_1]       =  0.0;     
    init_joint_angles[HEAD_YAW_1]         =  0.0;

    robot_interface_.reset(new oru_robot_interface(connection_ip.c_str(), connection_port, joint_names_));
    
    int index[joint_names_.size()];
    std::vector<double> joint_positions;
    joint_positions.resize(joint_names_.size());
     /* 
    for (int i=12; i< joint_names_.size(); i++) {
        index[i-12] = i;
        joint_positions[i-12] = init_joint_angles[i];
        //std::cout << joint_positions[i-12] << std::endl;
    }
   
    robot_interface_->sendPositionsCommand(index, joint_positions, joint_names_.size()/2);
    sleep(3);
    */
    
    for (int i=0; i< joint_names_.size(); i++) {
        index[i] = i;
        joint_positions[i] = init_joint_angles[i];
        //std::cout << joint_positions[i] << std::endl;
    }

    robot_interface_->sendPositionsCommand(index, joint_positions, joint_names_.size());
    sleep(3);
    

    std::vector<double> joint_positions_read;
    int index_read[JOINTS_NUM_1];
    for (int i= 0; i< joint_names_.size(); i++)
        index_read[i] = i;
    joint_positions_read.clear();
    robot_interface_->readJointPositions(index_read, JOINTS_NUM_1 , joint_positions_read);

    for (int i=0; i< JOINTS_NUM_1; i++) 
        std::cout << joint_names_[index_read[i]] << " : "<< joint_positions_read[i] << std::endl;
    std::cout << std::endl;
    sleep(1);

    return 0;
}
