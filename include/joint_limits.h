/**
 * @file
 * @author Alexander Sherikov
 * @date 02.12.2011 15:17:52 MSK
 */


#ifndef JOINT_LIMITS_H
#define JOINT_LIMITS_H
 #include <iostream>
#include <stdio.h>
#include <string>
#include <vector>

class JointLimits
{
    public:
        JointLimits();

    public:
        enum jointSensorIDs {
            // LEFT LEG
            L_HIP_ROLL          = 0 ,
            L_HIP_YAW           = 1 ,
            L_HIP_PITCH         = 2 ,
            L_KNEE_PITCH        = 3 ,
            L_ANKLE_PITCH       = 4 ,
            L_ANKLE_ROLL        = 5 ,

            // RIGHT LEG
            R_HIP_ROLL          = 6 ,
            R_HIP_YAW           = 7 ,
            R_HIP_PITCH         = 8 ,
            R_KNEE_PITCH        = 9 ,
            R_ANKLE_PITCH       = 10,
            R_ANKLE_ROLL        = 11,

            // The number of joints in the lower body.
            LOWER_JOINTS_NUM    = 12,

            // LEFT ARM
            L_SHOULDER_PITCH    = 12,
            L_SHOULDER_ROLL     = 13,
            L_ELBOW_ROLL        = 14,
            L_ELBOW_YAW         = 15,
            L_WRIST_YAW         = 16,

            // RIGHT ARM
            R_SHOULDER_PITCH    = 17,
            R_SHOULDER_ROLL     = 18,
            R_ELBOW_ROLL        = 19,
            R_ELBOW_YAW         = 20,
            R_WRIST_YAW         = 21,

            // HEAD
            HEAD_YAW            = 22,
            HEAD_PITCH          = 23,
            HEAD_ROLL           = 24,

            // TORSO
            TORSO_YAW           = 25,

            JOINTS_NUM          = 26
        };

        bool convertToModelValues(std::vector<double> positions_in, std::vector<double> &positions_out);
        bool convertToActualValues(std::vector<double> positions_in, std::vector<double> &positions_out);

        void convertToModelValue(double positions_in, double &positions_out, const JointLimits::jointSensorIDs id);
        void convertToActualValue(double positions_in, double &positions_out, const JointLimits::jointSensorIDs id);
    private:
        void initModelBounds();
        void initActualBounds();
        void setModelJointBounds(const JointLimits::jointSensorIDs, const double, const double, const double, const double);
        void setActualJointBounds(const JointLimits::jointSensorIDs, const double, const double);


        static double q_lower_bound_model[JointLimits::JOINTS_NUM];
        static double q_upper_bound_model[JointLimits::JOINTS_NUM];
        static double q_lower_bound_actual[JointLimits::JOINTS_NUM];
        static double q_upper_bound_actual[JointLimits::JOINTS_NUM];
        static double offsets_model[JointLimits::JOINTS_NUM];
        static double direction_model[JointLimits::JOINTS_NUM];
};


#endif // JOINT_LIMITS_H
