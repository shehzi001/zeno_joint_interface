/**
 *  zeno_joint_interface.h
 *
 *  Created on: 23 Oct, 2015
 *  Author: Shehzad
 */

#ifndef ZENOJOINTINTERFACE_H_
#define ZENOJOINTINTERFACE_H_

#include "JointInterface.h"
#include "joint_limits.h"
#include <zeno_data_communication_manager.h>

#include <iostream>
#include <string>
#include <vector>

#define DEG_TO_RAD 0.0174532925

class zenoJointInterface: public JointInterface {
private:

    int clientID;
    std::vector<int> handles;
    std::vector<std::string> joint_names_;
    boost::shared_ptr<DataCommunicationManager> data_communication_manager_;

public:
    zenoJointInterface(const std::vector<std::string> &joint_names);

    virtual ~zenoJointInterface();

    void setJointPosition(int index, double pos);

    void setJointPosition(int index[], std::vector<double> positions, int size);

    void setJointVelocity(int index, double vel);

    void setJointVelocity(int index[], std::vector<double> velocities, int size);

    double getJointPosition(int index);

    void getJointPosition(int index[], int size, std::vector<double> &positions);

    double getJointVelocity(int index);

    void getIMUData(const std::vector<std::string> &signal_names, std::vector<double> &imu_data);

    void shutdown();
};

#endif /* ZENOJOINTINTERFACE_H_ */
