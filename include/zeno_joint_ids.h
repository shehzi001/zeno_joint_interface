#ifndef ZENO_JOINT_IDS_H
#define ZENO_JOINT_IDS_H

/****************************************
 * TYPEDEFS 
 ****************************************/

enum zenoJointIds {
    // LEFT LEG
    L_HIP_ROLL_1          = 0 ,
    L_HIP_YAW_PITCH_1     = 1 ,
    L_HIP_PITCH_1         = 2 ,
    L_KNEE_PITCH_1        = 3 ,
    L_ANKLE_PITCH_1       = 4 ,
    L_ANKLE_ROLL_1        = 5 ,

    // RIGHT LEG
    R_HIP_ROLL_1          = 6 ,
    R_HIP_YAW_PITCH_1     = 7 ,
    R_HIP_PITCH_1         = 8 ,
    R_KNEE_PITCH_1        = 9 ,
    R_ANKLE_PITCH_1       = 10,
    R_ANKLE_ROLL_1        = 11,

    // The number of joints in the lower body.
    LOWER_JOINTS_NUM_1    = 12,

    // LEFT ARM
    L_SHOULDER_PITCH_1    = 12,
    L_SHOULDER_ROLL_1     = 13,
    L_ELBOW_ROLL_1        = 14,
    L_ELBOW_YAW_1         = 15,
    L_WRIST_YAW_1         = 16,

    // RIGHT ARM
    R_SHOULDER_PITCH_1    = 17,
    R_SHOULDER_ROLL_1     = 18,
    R_ELBOW_ROLL_1        = 19,
    R_ELBOW_YAW_1         = 20,
    R_WRIST_YAW_1         = 21,

    // HEAD
    HEAD_YAW_1            = 22,
    HEAD_PITCH_1          = 23,
    HEAD_ROLL_1            = 24,

    // TORSO
    TORSO_YAW_1           = 25,

    JOINTS_NUM_1          = 26
};
#endif // ZENO_JOINT_IDS_H
