/**
 * @file
 * @author Shehzad Ahmed
 */


#include <joint_limits.h>

double JointLimits::q_lower_bound_model[JointLimits::JOINTS_NUM];
double JointLimits::q_upper_bound_model[JointLimits::JOINTS_NUM];
double JointLimits::q_lower_bound_actual[JointLimits::JOINTS_NUM];
double JointLimits::q_upper_bound_actual[JointLimits::JOINTS_NUM];
double JointLimits::offsets_model[JointLimits::JOINTS_NUM];
double JointLimits::direction_model[JointLimits::JOINTS_NUM];

/**
 * @brief Constructor
 */
JointLimits::JointLimits()
{
    initModelBounds();
    initActualBounds();
}

/**
 * @brief Initialize bounds.
 */
void JointLimits::initModelBounds()
{
    // LEFT LEG
    setModelJointBounds(JointLimits::L_HIP_ROLL       ,   -0.7854,    0.7854,    1.0, 0.4363); // -0.6
    setModelJointBounds(JointLimits::L_HIP_YAW        ,   -0.34906,    1.5708,   -1.0,    0.0);
    setModelJointBounds(JointLimits::L_HIP_PITCH      ,   -1.4868,       0.0,    -1.0,    0.0); 
    setModelJointBounds(JointLimits::L_KNEE_PITCH     ,       0.0,    1.5708,    1.0,    0.0);
    setModelJointBounds(JointLimits::L_ANKLE_PITCH    ,   -0.7854,    1.3100,    -1.0,    0.0);
    setModelJointBounds(JointLimits::L_ANKLE_ROLL     ,   -0.4363,    0.4363,   -1.0,    0.0);
                              
    // RIGHT LEG
    setModelJointBounds(JointLimits::R_HIP_ROLL       ,   -0.7854,    0.7854,    1.0, 0.4363);
    setModelJointBounds(JointLimits::R_HIP_YAW        ,   -1.5708,    0.34906,   -1.0,   0.0); 
    setModelJointBounds(JointLimits::R_HIP_PITCH      ,   -1.4868,       0.0,   -1.0,    0.0);   
    setModelJointBounds(JointLimits::R_KNEE_PITCH     ,       0.0,    1.5708,    1.0,    0.0);
    setModelJointBounds(JointLimits::R_ANKLE_PITCH    ,   -0.7854,    1.3100,   -1.0,    0.0);
    setModelJointBounds(JointLimits::R_ANKLE_ROLL     ,   -0.4363,    0.4363,   -1.0,    0.0);
                              
    // LEFT ARM
    setModelJointBounds(JointLimits::L_SHOULDER_PITCH ,   -1.5708,    1.5708,    1.0,     0.0);
    setModelJointBounds(JointLimits::L_SHOULDER_ROLL  ,       0.0,    1.5708,    1.0, -1.5708);
    setModelJointBounds(JointLimits::L_ELBOW_ROLL     ,   -3.1416,       0.0,   1.0,  1.5708);
    setModelJointBounds(JointLimits::L_ELBOW_YAW      ,   -1.5708,       0.0,   -1.0,     0.0);
    setModelJointBounds(JointLimits::L_WRIST_YAW      ,       0.0,    1.5708,    1.0,     0.0);
                              
    // RIGHT ARM
    setModelJointBounds(JointLimits::R_SHOULDER_PITCH ,   -1.5708,    1.5708,   -1.0,     0.0);
    setModelJointBounds(JointLimits::R_SHOULDER_ROLL  ,   -1.5708,       0.0,   1.0,     1.57);
    setModelJointBounds(JointLimits::R_ELBOW_ROLL     ,         0,    3.1416,    1.0, -1.5708);
    setModelJointBounds(JointLimits::R_ELBOW_YAW      ,       0.0,    1.5708,    1.0,     0.0);
    setModelJointBounds(JointLimits::R_WRIST_YAW      ,       0.0,    1.5708,    1.0,     0.0);

    // HEAD
    setModelJointBounds(JointLimits::HEAD_YAW         ,   -1.5708,    1.5708,    1.0,    0.0);
    setModelJointBounds(JointLimits::HEAD_PITCH       ,         0,    0.5235,    1.0,    0.0);
    setModelJointBounds(JointLimits::HEAD_ROLL        ,   -0.5235,    0.5235,    1.0,    0.0);

    // TORSO
    setModelJointBounds(JointLimits::TORSO_YAW        ,   -1.0472,    1.0472,    1.0,    0.0);
}


/**
 * @brief Initialize bounds.
 */
void JointLimits::initActualBounds()
{
    // LEFT LEG
    setActualJointBounds(JointLimits::L_HIP_ROLL      ,   -0.3495,    1.2217); //0.4363,    1.2217);
    setActualJointBounds(JointLimits::L_HIP_YAW       ,   -1.5708,    0.3490);
    setActualJointBounds(JointLimits::L_HIP_PITCH     ,    0.0,       1.4868);
    setActualJointBounds(JointLimits::L_KNEE_PITCH    ,    0.0,       1.5708);
    setActualJointBounds(JointLimits::L_ANKLE_PITCH   ,   -1.310,     0.7854);
    setActualJointBounds(JointLimits::L_ANKLE_ROLL    ,   -0.4363,    0.4363);
                              
    // RIGHT LEG
    setActualJointBounds(JointLimits::R_HIP_ROLL      ,   -0.3491,    1.2217); //-0.3491,    0.4363);
    setActualJointBounds(JointLimits::R_HIP_YAW       ,   -0.3490,    1.5708);
    setActualJointBounds(JointLimits::R_HIP_PITCH     ,       0.0,    1.4868);
    setActualJointBounds(JointLimits::R_KNEE_PITCH    ,       0.0,    1.5708);
    setActualJointBounds(JointLimits::R_ANKLE_PITCH   ,   -1.3100,    0.7854);
    setActualJointBounds(JointLimits::R_ANKLE_ROLL    ,   -0.4363,    0.4363);
                              
    // LEFT ARM
    setActualJointBounds(JointLimits::L_SHOULDER_PITCH,   -1.5708,    1.5708);
    setActualJointBounds(JointLimits::L_SHOULDER_ROLL ,   -1.5708,    0.0);
    setActualJointBounds(JointLimits::L_ELBOW_ROLL    ,   -1.5708,    1.5708);
    setActualJointBounds(JointLimits::L_ELBOW_YAW     ,       0.0,    1.5708);
    setActualJointBounds(JointLimits::L_WRIST_YAW     ,       0.0,    1.5708);
                              
    // RIGHT ARM
    setActualJointBounds(JointLimits::R_SHOULDER_PITCH,   -1.5708,    1.5708);
    setActualJointBounds(JointLimits::R_SHOULDER_ROLL ,   -0.1049,    1.5176);
    setActualJointBounds(JointLimits::R_ELBOW_ROLL    ,   -1.5708,    1.5708);
    setActualJointBounds(JointLimits::R_ELBOW_YAW     ,       0.0,    1.5708);
    setActualJointBounds(JointLimits::R_WRIST_YAW     ,         0,    1.5708);

    // HEAD
    setActualJointBounds(JointLimits::HEAD_YAW        ,   -1.5708,    1.5708);
    setActualJointBounds(JointLimits::HEAD_PITCH      ,         0,    0.5235);
    setActualJointBounds(JointLimits::HEAD_ROLL       ,   -0.5235,    0.5235);

    // TORSO
    setActualJointBounds(JointLimits::TORSO_YAW        ,  -1.046,    1.046);
}

void JointLimits::setModelJointBounds(const JointLimits::jointSensorIDs id, 
                                        const double lower_bound, 
                                        const double upper_bound,
                                        const double direction,
                                        const double offset)
{
    q_lower_bound_model[id] = lower_bound;
    q_upper_bound_model[id] = upper_bound;
    direction_model[id] = direction;
    offsets_model[id]  = offset;
}


void JointLimits::setActualJointBounds(const JointLimits::jointSensorIDs id,
                                        const double lower_bound, 
                                        const double upper_bound) 
{
    q_lower_bound_actual[id] = lower_bound;
    q_upper_bound_actual[id] = upper_bound;
}


void JointLimits::convertToModelValue(double positions_in, double &positions_out, const JointLimits::jointSensorIDs id)
{
    positions_out = (1/direction_model[id])*(positions_in) - offsets_model[id];
}

void JointLimits::convertToActualValue(double positions_in, double &positions_out, const JointLimits::jointSensorIDs id)
{
    positions_out = direction_model[id]*(positions_in + offsets_model[id]);
    //std::cout << id << ":" << positions_out << std::endl;
}

bool JointLimits::convertToModelValues(std::vector<double> positions_in, std::vector<double> &positions_out)
{
    if(positions_in.empty()) return false;
    positions_out.resize(positions_in.size());
    for(int i=0; i < positions_in.size(); i++) {
        convertToModelValue(positions_in[i], positions_out[i], static_cast<JointLimits::jointSensorIDs>(i));
    }
    return true;

}

bool JointLimits::convertToActualValues(std::vector<double> positions_in, std::vector<double> &positions_out)
{
    if(positions_in.empty()) return false;
    positions_out.resize(positions_in.size());
    for(int i=0; i < positions_in.size(); i++) {
        convertToActualValue(positions_in[i], positions_out[i], static_cast<JointLimits::jointSensorIDs>(i));
    }
    return true;
}
