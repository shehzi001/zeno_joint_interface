
#include "oru_robot_interface.h"

/**
 * Ctor.
 */
oru_robot_interface::oru_robot_interface(const char* connection_ip, int connection_port, std::vector<string> joint_names):
        no_of_imu_signals(7), pitch_g(0), roll_g(0),pitch_a(0), roll_a(0), pitch_imu(0), roll_imu(0)
{
    zeno_joint_interface_.reset(new zenoJointInterface(joint_names));

    imu_signal_names.resize(no_of_imu_signals);
    imu_data_buffer.resize(no_of_imu_signals);

    imu_signal_names[0] = "accel-X";
    imu_signal_names[1] = "accel-Y";
    imu_signal_names[2] = "accel-Z";
    imu_signal_names[3] = "gyro-X";
    imu_signal_names[4] = "gyro-Y";
    imu_signal_names[5] = "gyro-Z";
    imu_signal_names[6] = "gyro-dt";
}

/**
 * Dtor.
 */
oru_robot_interface::~oru_robot_interface ()
{
    zeno_joint_interface_->shutdown();
}

/**
 * Sends position commands to joint position controller.
 */
void oru_robot_interface::sendPositionsCommand(int index[], std::vector<double> positions, int size)
{
    zeno_joint_interface_->setJointPosition(index, positions, size);
}

/**
 * Sends position commands to joint position controller.
 */
void oru_robot_interface::sendVelocityCommand(int index[], std::vector<double> velocities, int size)
{
    zeno_joint_interface_->setJointVelocity(index, velocities, size);
}


/**
 * Reads current joint positions.
 */
void oru_robot_interface::readJointPositions(int index[],  int size, std::vector<double> &positions)
{
    zeno_joint_interface_->getJointPosition(index, size, positions);
}

/**
 * Reads current joint positions.
 */
void oru_robot_interface::getJointVelocity()
{
    zeno_joint_interface_->getJointVelocity(0);
}

void oru_robot_interface::getIMUData(std::vector<double> &imu_data)
{
}

void oru_robot_interface::getTorsoFeedback(std::vector<double> &torso_feedback)
{
    zeno_joint_interface_->getIMUData(imu_signal_names, torso_feedback);
}
