#include "vrep/zenoJointInterface.h"

#include <iostream>
#include <string>
#include <vector>


zenoJointInterface::zenoJointInterface(const std::vector<std::string> &joint_names)
{
    joint_names_.resize(JointLimits::JOINTS_NUM);    
    joint_names_[JointLimits::HEAD_PITCH]       = "neck_pitch";
    joint_names_[JointLimits::HEAD_YAW]         = "neck_yaw";
    joint_names_[JointLimits::HEAD_ROLL]        = "neck_roll";

    joint_names_[JointLimits::L_ANKLE_PITCH]    = "left_ankle_pitch";
    joint_names_[JointLimits::L_ANKLE_ROLL]     = "left_ankle_roll";
    joint_names_[JointLimits::L_ELBOW_ROLL]     = "left_elbow_roll";
    joint_names_[JointLimits::L_ELBOW_YAW]      = "left_elbow_yaw";
    joint_names_[JointLimits::L_HIP_PITCH]      = "left_hip_pitch";
    joint_names_[JointLimits::L_HIP_ROLL]       = "left_hip_roll";
    joint_names_[JointLimits::L_HIP_YAW]        = "left_hip_yaw";
    joint_names_[JointLimits::L_KNEE_PITCH]     = "left_knee_pitch";
    joint_names_[JointLimits::L_SHOULDER_PITCH] = "left_shoulder_pitch";
    joint_names_[JointLimits::L_SHOULDER_ROLL]  = "left_shoulder_roll";
    joint_names_[JointLimits::L_WRIST_YAW]      = "left_wrist_yaw";

    joint_names_[JointLimits::R_ANKLE_PITCH]    = "right_ankle_pitch";
    joint_names_[JointLimits::R_ANKLE_ROLL]     = "right_ankle_roll";
    joint_names_[JointLimits::R_ELBOW_ROLL]     = "right_elbow_roll";
    joint_names_[JointLimits::R_ELBOW_YAW]      = "right_elbow_yaw";
    joint_names_[JointLimits::R_HIP_PITCH]      = "right_hip_pitch";
    joint_names_[JointLimits::R_HIP_ROLL]       = "right_hip_roll";
    joint_names_[JointLimits::R_HIP_YAW]        = "right_hip_yaw";
    joint_names_[JointLimits::R_KNEE_PITCH]     = "right_knee_pitch";
    joint_names_[JointLimits::R_SHOULDER_PITCH] = "right_shoulder_pitch";
    joint_names_[JointLimits::R_SHOULDER_ROLL]  = "right_shoulder_roll";
    joint_names_[JointLimits::R_WRIST_YAW]      = "right_wrist_yaw";

    joint_names_[JointLimits::TORSO_YAW]         = "waist";

    data_communication_manager_.reset(
                                new DataCommunicationManager(
                                    "tcp://*:5556", 
                                    "tcp://192.168.188.27:5557"));
    data_communication_manager_->initialize();
}


zenoJointInterface::~zenoJointInterface() {

}

void zenoJointInterface::setJointPosition(int index, double pos) {

}

void zenoJointInterface::setJointPosition(int index[], std::vector<double> positions, int size) {

    std::vector<std::string> body_joint_names;
    std::vector<double> body_joint_values;

    for (int i=0; i < size; i++) {
        if ((index[i] != JointLimits::R_WRIST_YAW) && (index[i] != JointLimits::L_WRIST_YAW)) {
                body_joint_names.push_back(joint_names_[index[i]]);
                body_joint_values.push_back(positions[i]);
        }
    }

    //call body joint poisition command method
    if(body_joint_values.size() > 0) {
        data_communication_manager_->set_joint_position_commands(body_joint_names, body_joint_values);
    }
}

void zenoJointInterface::setJointVelocity(int index, double vel) {

}

void zenoJointInterface::setJointVelocity(int index[], std::vector<double> velocities, int size) {

	for (int i=0; i < size; i++) {
		setJointVelocity(index[i], velocities[i]);
	}
}

double zenoJointInterface::getJointPosition(int index) {

}

void zenoJointInterface::getJointPosition(int index[], int size, std::vector<double> &positions) {

    std::vector<std::string> body_joint_names;

    for (int i=0; i < size; i++) {
        if ((index[i] != JointLimits::R_WRIST_YAW) && (index[i] != JointLimits::L_WRIST_YAW)) {
                body_joint_names.push_back(joint_names_[index[i]]);
        }
    }

    //call body joint poisition command method
    std::vector<double> body_joint_values;
    if(body_joint_names.size() > 0) {
        bool success = data_communication_manager_->get_current_joint_angles(body_joint_names, body_joint_values);
    }

    positions.resize(size);

    for (int i=0; i < size; i++) {
        if ((index[i] != JointLimits::R_WRIST_YAW) && (index[i] != JointLimits::L_WRIST_YAW)) {
                int pos = find(body_joint_names.begin(), body_joint_names.end(), joint_names_.at(index[i])) - body_joint_names.begin();
                positions[i] = body_joint_values[pos];
        }
    }
}

double zenoJointInterface::getJointVelocity(int index) {

}

void zenoJointInterface::getIMUData(const std::vector<std::string> &signal_names, std::vector<double> &imu_data)
{
    bool success = data_communication_manager_->get_imu_data(imu_data);
    for (int i=0; i < imu_data.size(); i++) {
        imu_data[i] = imu_data[i]*DEG_TO_RAD;
    }
}

void zenoJointInterface::shutdown()
{
    data_communication_manager_->shutdown();
    data_communication_manager_->shutdown();
}
