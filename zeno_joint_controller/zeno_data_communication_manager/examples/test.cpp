#include <stdio.h>
#include <string>
#include <errno.h>
#include <limits.h>
#include <assert.h>
#include <stdlib.h>
#include <vector>
#include <map>
#include <zeno_body_controller_wrapper.h>
#include <zeno_head_controller_wrapper.h>
#include <Python.h>

#include <iostream>

class test
{
    public:
        test()
        {
            // create a new interpreter 
            PyEval_AcquireLock() ; // nb: get the GIL
            pThreadState = Py_NewInterpreter() ;
            assert( pThreadState != NULL ) ;
            PyEval_ReleaseThread( pThreadState ) ; // nb: this also releases the GIL
        }

        ~test()
        {

        }

        void print_test(std::string msg)
        {
            PyEval_AcquireThread( pThreadState );

            std::cout << msg << std::endl;

            PyEval_ReleaseThread( pThreadState );
        }

        void shutdown()
        {
            // release the interpreter 
            PyEval_AcquireThread( pThreadState ) ; // nb: this also locks the GIL
            Py_EndInterpreter( pThreadState ) ;
            PyEval_ReleaseLock() ; // nb: release the GIL
        }

    private:
        PyThreadState* pThreadState;
};

int main( int argc , char** argv )
{
    // initialize Python
    Py_Initialize() ;
    PyEval_InitThreads() ; // nb: creates and locks the GIL
    // NOTE: We save the current thread state, and restore it when we unload,
    // so that we can clean up properly.
    PyThreadState* pMainThreadState = PyEval_SaveThread() ; // nb: this also releases the GIL


    test test1 = test();
    test1.print_test("test 1");

    test test2 = test();
    test2.print_test("test 2");

    test test3 = test();
    test3.print_test("test 3");

    test1.shutdown();
    test2.shutdown();
    test3.shutdown();
 
 
    // clean up
    PyEval_RestoreThread( pMainThreadState ) ; // nb: this also locks the GIL

    std::cout << "py finalize before" << std::endl;

    Py_Finalize() ;
    std::cout << "py finalize after" << std::endl;

    return 1;
}