#include <stdio.h>
#include <string>
#include <errno.h>
#include <limits.h>
#include <assert.h>
#include <stdlib.h>
#include <vector>
#include <map>
#include <zeno_body_controller_wrapper.h>
#include <zeno_head_controller_wrapper.h>
#include <Python.h>

int main(int argc, char *argv[])
{
    char* body_module_name = "zeno_body_controller";

    char* body_class_name = "ZenobodyController";

    char* head_module_name = "zeno_head_controller";

    char* head_class_name = "ZenoHeadController";

    std::vector<std::string> names;

    names.resize(2);

    names[0] = "left_hip_pitch";

    names[1] = "left_hip_roll";

    std::vector<std::string> head_joint_names;

    head_joint_names.resize(2);

    head_joint_names[0] = "neck_pitch";

    head_joint_names[1] = "neck_roll";
    // initialize Python
    Py_Initialize() ;
    PyEval_InitThreads() ; // nb: creates and locks the GIL
    // NOTE: We save the current thread state, and restore it when we unload,
    // so that we can clean up properly.
    PyThreadState* pMainThreadState = PyEval_SaveThread() ; // nb: this also releases the GIL

    ZenoBodyControllerWrapper  zeno_body_controller_wrapper = ZenoBodyControllerWrapper(body_module_name, body_class_name);

    zeno_body_controller_wrapper.initialization();

    ZenoBodyControllerWrapper  zeno_head_controller_wrapper =  ZenoBodyControllerWrapper(head_module_name, head_class_name);

    zeno_head_controller_wrapper.initialization();
    
    std::vector<double> current_positions;

    bool success = zeno_body_controller_wrapper.get_current_joint_angles(names, current_positions);

    if (success) {
        printf("current joint success \n");
        for(int i=0; i< current_positions.size(); i++) {
            std::cout << current_positions[i] << " ";
        }
        std::cout << std::endl;
    }
    
    std::vector<double> head_current_positions;

    bool head_success = zeno_head_controller_wrapper.get_current_joint_angles(head_joint_names, head_current_positions);

    if (head_success) {
        printf("current head joint success \n");
        for(int i=0; i< head_current_positions.size(); i++) {
            std::cout << head_current_positions[i] << " ";
        }
        std::cout << std::endl;
    }

    //zeno_body_controller_wrapper.shutdown();
    zeno_head_controller_wrapper.shutdown();
 
    // clean up
    PyEval_RestoreThread( pMainThreadState ) ; // nb: this also locks the GIL

    std::cout << "py finalize before" << std::endl;

    Py_Finalize() ;

    return 0;
}
