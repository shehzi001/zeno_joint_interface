#ifndef DATA_COMMUNICATION_MANAGER_H
#define DATA_COMMUNICATION_MANAGER_H

#include <iostream>
#include <string>
#include <boost/shared_ptr.hpp>
#include <vector>

#include <zmq.hpp>
#include <json/json.h>

class DataCommunicationManager
{
    public:
        DataCommunicationManager(std::string robot_address, std::string remote_host_address);

        virtual ~DataCommunicationManager();

        bool initialize();

        bool set_joint_position_commands(std::vector<std::string> joint_names, std::vector<double> joint_angles);

        bool set_joint_velocity_commands(std::vector<std::string> joint_names, std::vector<double> joint_speeds);

        bool get_current_joint_angles(std::vector<std::string> joint_names, std::vector<double> &current_positions);

        bool get_imu_data(std::vector<double> &imu);

        void shutdown();

    private:
        /**
         * Copy Ctor.
         */
        DataCommunicationManager(const DataCommunicationManager &other);

        /**
         * Assignment operator
         */
        DataCommunicationManager &operator=(const DataCommunicationManager &other);

    private:
        Json::Value iterable2json(std::vector<std::string> vec) {
            Json::Value v;
            for(int i=0; i < vec.size(); i++)
                v.append(vec[i]);
             return v;
        }

        Json::Value iterable2json(std::vector<double> vec) {
            Json::Value v;
            for(int i=0; i < vec.size(); i++)
                v.append(vec[i]);
             return v;
        }

        void publish_command(Json::Value command) {
            // Prepare Json message
            Json::FastWriter fastWriter;
            std::string jsonMessage = fastWriter.write(command);

            //  Prepare zmq message
            zmq::message_t message(jsonMessage.length());
            memcpy(message.data(), jsonMessage.c_str(), jsonMessage.length());
            command_pub_->send(message);
        }

        bool subscribe_joint_states(std::vector<double> &joint_states) {
            zmq::message_t joint_states_zmq;
            
            //waiting for joint states
            joint_states_sub_->recv(&joint_states_zmq); //fix me wait forever?

            // convert non null terminating cstring into a string
            std::string response = std::string(
                    static_cast<const char*>(joint_states_zmq.data()),
                    joint_states_zmq.size());

            // parse json data
            Json::Value v, j_values;
            Json::Reader reader;
            bool parsingSuccessful = reader.parse(response, v);
            if(!parsingSuccessful)
            {
                return false;
            }

            j_values = v["joint_states"];
            joint_states.resize(j_values.size());
            for(int i=0; i < j_values.size(); i++)
                joint_states[i] = j_values[i].asDouble();

            return true;
        }

        bool subscribe_imu(std::vector<double> &imu_angles) {
            zmq::message_t imu_angles_zmq;
            
            //waiting for joint states
            imu_sub_->recv(&imu_angles_zmq); //fix me wait forever?

            // convert non null terminating cstring into a string
            std::string response = std::string(
                    static_cast<const char*>(imu_angles_zmq.data()),
                    imu_angles_zmq.size());

            // parse json data
            Json::Value v, j_angles;
            Json::Reader reader;
            bool parsingSuccessful = reader.parse(response, v);
            if(!parsingSuccessful)
            {
                return false;
            }

            j_angles = v["torso_angle"];
            imu_angles.resize(j_angles.size());
            for(int i=0; i < j_angles.size(); i++)
                imu_angles[i] = j_angles[i].asDouble();

            return true;
        }

    private:
        std::string robot_address_;
        std::string remote_host_address_;
        boost::shared_ptr<zmq::socket_t> command_pub_;
        boost::shared_ptr<zmq::socket_t> joint_states_sub_;
        boost::shared_ptr<zmq::socket_t> imu_sub_;
        zmq::context_t context_command;
        zmq::context_t context_joint_states;
        zmq::context_t context_imu;
};
#endif /*DATA_COMMUNICATION_MANAGER_H*/