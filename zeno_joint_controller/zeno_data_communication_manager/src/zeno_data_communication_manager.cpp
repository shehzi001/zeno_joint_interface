
#include <zeno_data_communication_manager.h>


DataCommunicationManager::DataCommunicationManager(std::string robot_address, std::string remote_host_address):
    robot_address_(robot_address), remote_host_address_(remote_host_address), context_command(1),
     context_joint_states(1), context_imu(1)
{
}

DataCommunicationManager::~DataCommunicationManager()
{
}

bool DataCommunicationManager::initialize()
{
    //  Prepare our context and command_pub
    //zmq::socket_t command_pub(context, ZMQ_PUB);
    command_pub_.reset(new zmq::socket_t(context_command, ZMQ_PUB));
   
    //std::string robot_address("tcp://*:5556");
    command_pub_->bind(robot_address_.c_str());

    //zmq::socket_t joint_states_sub(context1, ZMQ_SUB);
    //std::string remote_host_address("tcp://localhost:5557");

    joint_states_sub_.reset(new zmq::socket_t(context_joint_states, ZMQ_SUB));
    joint_states_sub_->connect(remote_host_address_.c_str());
    // subscribe to filter
    std::string filter = "";
    joint_states_sub_->setsockopt(ZMQ_SUBSCRIBE, filter.c_str(), filter.length());
    
    imu_sub_.reset(new zmq::socket_t(context_imu, ZMQ_SUB));
    imu_sub_->connect("tcp://192.168.188.27:6000");
    // subscribe to filter

    imu_sub_->setsockopt(ZMQ_SUBSCRIBE, filter.c_str(), filter.length());
    
    std::cout<< "DataCommunicationManager initialized." << std::endl;

    return true;
}
/*
bool vec2json(std::vector<std::string> v1, Json::Value &v2)
{
    if (v1.empty())
        return false;
    
    for(int i=0; i<v1.size(); i++)
        v2.append(v1[i]);
     return true;
}
*/


bool DataCommunicationManager::set_joint_position_commands(std::vector<std::string> joint_names,
                                                            std::vector<double> joint_angles)
{
    Json::Value j_names = iterable2json(joint_names);
    Json::Value j_values = iterable2json(joint_angles);

    Json::Value command;
    // Fill command
    command["op"] = "position_command";
    command["joint_names"] = j_names;
    command["joint_positions"] = j_values;

    publish_command(command);

    return true;
}

bool DataCommunicationManager::set_joint_velocity_commands(std::vector<std::string> joint_names,
                                                            std::vector<double> joint_speeds)
{
    Json::Value j_names = iterable2json(joint_names);
    Json::Value j_speeds = iterable2json(joint_speeds);

    Json::Value command;
    // Fill command
    command["op"] = "velocity_command";
    command["joint_names"] = j_names;
    command["joint_velocities"] = j_speeds;

    publish_command(command);

    return true;
}

bool DataCommunicationManager::get_current_joint_angles(std::vector<std::string> joint_names, std::vector<double> &current_positions) 
{
    Json::Value j_names = iterable2json(joint_names);

    Json::Value command;
    // Fill command
    command["op"] = "joint_states";
    command["joint_names"] = j_names;

    publish_command(command);

    if(!subscribe_joint_states(current_positions))
        return false;

    return true;
}

bool DataCommunicationManager::get_imu_data(std::vector<double> &imu) 
{
    if(!subscribe_imu(imu))
        return false;

    return true;
}

void DataCommunicationManager::shutdown()
{
    command_pub_->close();
}
